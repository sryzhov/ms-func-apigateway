const path = require("path");
const webpack = require("webpack");

function isSubpathPresent( subpath ){
	return function( module ){
		const userRequest = module.userRequest;
		if( typeof userRequest !== "string"){
			return false;
		}
		return userRequest.indexOf("node_modules") >= 0 && userRequest.indexOf(subpath) >= 0;
	}
}

module.exports = {
	entry: {
		vendor: [
			"babel-polyfill",
			"es6-promise/auto",
			"promise.prototype.finally",
			"fetch-ie8"
		],
		app: [
			"babel-polyfill",
			"es6-promise/auto",
			"promise.prototype.finally",
			"fetch-ie8",
			"./src/index.js"		
		]
	},
	output: {
		publicPath: "assets/",
		path: path.join(__dirname, "dist/assets"),
		filename: "[name].js"
	},
	module: {
		rules: [
			{
				test: /\.(js|jsx)$/,
				exclude: /(node_modules)/,
				loader: "eslint-loader",
				enforce: "pre",
				options: {
					fix: true
				}
			},
			{
				test: /\.(js|jsx)$/,
				exclude: /(node_modules)/,
				loader: "babel-loader",
				options: {
					presets: [ "@babel/env", "@babel/preset-react" ]
				},
			},
			{
				test: /\.css$/,
				use: [
					{
            					loader: 'style-loader'
        				},
        				{
            					loader: 'css-loader',
            					options: {
                					importLoaders: 1,
                					modules: {
                    						localIdentName: '[name]__[local]___[hash:base64:5]'
                					}
            					}
        				}
				],
			},
			{
				test: /\.less$/,
				use: [
					{
            					loader: 'style-loader'
        				},
        				{
            					loader: 'css-loader',
            					options: {
                					importLoaders: 1,
                					modules: {
                    						localIdentName: '[name]__[local]___[hash:base64:5]',
                					}
            					}
        				},
					{
            					loader: 'less-loader'
        				}
				],
			},
			{
				test: /\.json$/,
				loader: "json-loader",
			},
			{
				test: /\.(eot|gif|jpeg|jpg|png|svg|ttf|woff|woff2)(\?[0-9]+)?$/,
				loader: "url-loader",
				options: {
					limit: 32767,
				}
			},
			{
				test: /\.md$/,
				use: [
					{
						loader: "html-loader"
					},
					{
						loader: "markdown-loader",
						options: {}
					}
				]
            		}
		]
	},
	externals: {},
	target: "web",
	plugins: [
		new webpack.DefinePlugin({
			__DEBUG__: JSON.stringify( JSON.parse( process.env.DEBUG || "false" )),
			__SERVER_PATH__: JSON.stringify("../apigateway")
		}),
	],
	resolve: {
		alias: {
			moment$: "moment/moment.js",
			src: path.resolve(__dirname, "src")
		}
	},
	devServer: {
		clientLogLevel: "info",
		compress: false,
		contentBase: "dist",
		historyApiFallback: {
			index: "index.html",
		},
		hot: true,
		inline: true,
		open: true,
		port: 9090,
		proxy: {
			"/api": "http://localhost:8088"
		},
		stats: {
			colors: true
		},
		watchContentBase: true,
	},
	devtool: "inline-source-map"
};