module.exports = {
    'env': {
        'browser': true,
        'commonjs': true,
        'es6': true
    },
    'extends': [
        'eslint:recommended',
        'plugin:react/recommended'
    ],
    'globals': {
        '__DEBUG__': false,
        'HTTPBRIDGE_SERVER_PATH': false,
        'SERVER_MODULE_NAME': false
    },
    parser: 'babel-eslint',
    'parserOptions': {
        'ecmaFeatures': {
            'experimentalObjectRestSpread': true,
            'jsx': true
        },
        'sourceType': 'module'
    },
    'plugins': [
        'import',
        'react'
    ],
    'rules': {
        // Possible Errors
        // There rules relate to possible syntax or login errors in JavaScript code:

        // disallow await inside of loops
        'no-await-in-loop': 2,
        // disallow comparing against -0
        'no-compare-neg-zero': 2,
        // disallow assignment operators in conditional expressions
        'no-cond-assign': 2,
        // disallow the use of console
        'no-console': 0,
        // disallow constant expressions in conditions
        'no-constant-condition': 2,
        // disallow control characters in regular expressions
        'no-control-regex': 2,
        // disallow the use of debugger
        'no-debugger': 1,
        // disallow duplicate arguments in function definitions
        'no-dupe-args': 2,
        // disallow duplicate keys in object literals
        'no-dupe-keys': 2,
        // disallow duplicate case labels
        'no-duplicate-case': 2,
        // disallow empty block statements
        'no-empty': 2,
        // disallow empty character classes in regular expressions
        'no-empty-character-class': 2,
        // disallow reassigning exceptions in catch clauses
        'no-ex-assign': 2,
        // disallow unnecessary boolean casts
        'no-extra-boolean-cast': 2,
        // disallow unnecessary parentheses
        'no-extra-parens': 2,
        // disallow unnecessary semicolons
        'no-extra-semi': 2,
        // disallow reassigning function declarations
        'no-func-assign': 2,
        // disallow variable or function declarations in nested blocks
        'no-inner-declarations': 2,
        // disallow invalid regular expression strings in RegExp constructors
        'no-invalid-regexp': 2,
        // disallow irregular whitespace outside of strings and comments
        'no-irregular-whitespace': 2,
        // disallow calling global object properties as functions
        'no-obj-calls': 2,
        // disallow calling some Object.prototype methods directly on objects
        'no-prototype-builtins': 2,
        // disallow multiple spaces in regular expressions
        'no-regex-spaces': 2,
        // disallow sparse arrays
        'no-sparse-arrays': 2,
        // disallow template literal placeholder syntax in regular strings
        'no-template-curly-in-string': 2,
        // disallow confusing multiline expressions
        'no-unexpected-multiline': 2,
        // disallow unreachable code after return, throw, continue, and break statements
        'no-unreachable': 2,
        // disallow control flow statements in finally blocks
        'no-unsafe-finally': 2,
        // disallow negating the left operand of relational operators
        'no-unsafe-negation': 2,
        // require calls to isNaN() when checking for NaN
        'use-isnan': 2,
        // enforce valid JSDoc comments
        'valid-jsdoc': 2,
        // enforce comparing typeof expressions against valid strings
        'valid-typeof': 2,

        // Best Practices
        // These rules relate to better ways of doing things to help you avoid problems:

        // enforce getter and setter pairs in objects
        'accessor-pairs': 2,
        // enforce return statements in callbacks of array methods
        'array-callback-return': 2,
        // enforce the use of variables within the scope they are defined
        'block-scoped-var': 2,
        // enforce that class methods utilize this
        'class-methods-use-this': [2, {'exceptMethods': ['render','shouldComponentUpdate']}],
        // enforce a maximum cyclomatic complexity allowed in a program
        'complexity': 2,
        // require return statements to either always or never specify values
        'consistent-return': 2,
        // enforce consistent brace style for all control statements
        'curly': 2,
        // require default cases in switch statements
        'default-case': 2,
        // enforce consistent newlines before and after dots
        'dot-location': [2, 'property'],
        // enforce dot notation whenever possible
        'dot-notation': 2,
        // require the use of === and !==
        'eqeqeq': 2,
        // require for-in loops to include an if statement
        'guard-for-in': 2,
        // disallow the use of alert, confirm, and prompt
        'no-alert': 2,
        // disallow the use of arguments.caller or arguments.callee
        'no-caller': 2,
        // disallow lexical declarations in case clauses
        'no-case-declarations': 2,
        // disallow division operators explicitly at the beginning of regular expressions
        'no-div-regex': 2,
        // disallow else blocks after return statements in if statements
        'no-else-return': 2,
        // disallow empty functions
        'no-empty-function': 2,
        // disallow empty destructuring patterns
        'no-empty-pattern': 2,
        // disallow null comparisons without type-checking operators
        'no-eq-null': 2,
        // disallow the use of eval()
        'no-eval': 2,
        // disallow extending native types
        'no-extend-native': 2,
        // disallow unnecessary calls to .bind()
        'no-extra-bind': 2,
        // disallow unnecessary labels
        'no-extra-label': 2,
        // disallow fallthrough of case statements
        'no-fallthrough': 2,
        // disallow leading or trailing decimal points in numeric literals
        'no-floating-decimal': 2,
        // disallow assignments to native objects or read-only global variables
        'no-global-assign': 2,
        // disallow shorthand type conversions
        'no-implicit-coercion': 2,
        // disallow variable and function declarations in the global scope
        'no-implicit-globals': 2,
        // disallow the use of eval()-like methods
        'no-implied-eval': 2,
        // disallow this keywords outside of classes or class-like objects
        'no-invalid-this': 2,
        // disallow the use of the __iterator__ property
        'no-iterator': 2,
        // disallow labeled statements
        'no-labels': 2,
        // disallow unnecessary nested blocks
        'no-lone-blocks': 2,
        // disallow function declarations and expressions inside loop statements
        'no-loop-func': 2,
        // disallow magic numbers
        'no-magic-numbers': 0,
        // disallow multiple spaces
        'no-multi-spaces': 2,
        // disallow multiline strings
        'no-multi-str': 2,
        // disallow new operators outside of assignments or comparisons
        'no-new': 2,
        // disallow new operators with the Function object
        'no-new-func': 2,
        // disallow new operators with the String, Number, and Boolean objects
        'no-new-wrappers': 2,
        // disallow octal literals
        'no-octal': 2,
        // disallow octal escape sequences in string literals
        'no-octal-escape': 2,
        // disallow reassigning function parameters
        'no-param-reassign': 2,
        // disallow the use of the __proto__ property
        'no-proto': 2,
        // disallow variable redeclaration
        'no-redeclare': 2,
        // disallow certain properties on certain objects
        'no-restricted-properties': 2,
        // disallow assignment operators in return statements
        'no-return-assign': 2,
        // disallow unnecessary return await
        'no-return-await': 2,
        // disallow javascript: urls
        'no-script-url': 2,
        // disallow assignments where both sides are exactly the same
        'no-self-assign': 2,
        // disallow comparisons where both sides are exactly the same
        'no-self-compare': 2,
        // disallow comma operators
        'no-sequences': 2,
        // disallow throwing literals as exceptions
        'no-throw-literal': 2,
        // disallow unmodified loop conditions
        'no-unmodified-loop-condition': 2,
        // disallow unused expressions
        'no-unused-expressions': 0,
        // disallow unused labels
        'no-unused-labels': 2,
        // disallow unnecessary calls to .call() and .apply()
        'no-useless-call': 2,
        // disallow unnecessary concatenation of literals or template literals
        'no-useless-concat': 2,
        // disallow unnecessary escape characters
        'no-useless-escape': 2,
        // disallow redundant return statements
        'no-useless-return': 2,
        // disallow void operators
        'no-void': 2,
        // disallow specified warning terms in comments
        'no-warning-comments': 1,
        // disallow with statements
        'no-with': 2,
        // require using Error objects as Promise rejection reasons
        'prefer-promise-reject-errors': 2,
        // enforce the consistent use of the radix argument when using parseInt()
        'radix': 2,
        // disallow async functions which have no await expression
        'require-await': 2,
        // require var declarations be placed at the top of their containing scope
        'vars-on-top': 2,
        // require parentheses around immediate function invocations
        'wrap-iife': 2,
        // require or disallow “Yoda” conditions
        'yoda': 2,

        // Strict Mode
        // These rules relate to strict mode directives:

        // require or disallow strict mode directives
        'strict': 0,

        // Variables
        // These rules relate to variable declarations:

        // require or disallow initialization in variable declarations
        'init-declarations': 0,
        // disallow catch clause parameters from shadowing variables in the outer scope
        'no-catch-shadow': 2,
        // disallow deleting variables
        'no-delete-var': 2,
        // disallow labels that share a name with a variable
        'no-label-var': 2,
        // disallow specified global variables
        'no-restricted-globals': 2,
        // disallow variable declarations from shadowing variables declared in the outer scope
        'no-shadow': 2,
        // disallow identifiers from shadowing restricted names
        'no-shadow-restricted-names': 2,
        // disallow the use of undeclared variables unless mentioned in /*global */ comments
        'no-undef': 2,
        // disallow initializing variables to undefined
        'no-undef-init': 2,
        // disallow the use of undefined as an identifier
        'no-undefined': 2,
        // disallow unused variables
        'no-unused-vars': 1,
        // disallow the use of variables before they are defined
        'no-use-before-define': [2, {'functions': false, 'classes': false}],

        // Node.js and CommonJS
        //  These rules relate to code running in Node.js, or in browsers with CommonJS:

        // require return statements after callbacks
        'callback-return': 0,
        // require require() calls to be placed at top-level module scope
        'global-require': 2,
        // require error handling in callbacks
        'handle-callback-err': 2,
        // disallow require calls to be mixed with regular variable declarations
        'no-mixed-requires': 2,
        // disallow new operators with calls to require
        'no-new-require': 2,
        // disallow string concatenation with __dirname and __filename
        'no-path-concat': 2,
        // disallow the use of process.env
        'no-process-env': 2,
        // disallow the use of process.exit()
        'no-process-exit': 2,
        // disallow specified modules when loaded by require
        'no-restricted-modules': 2,
        // disallow synchronous methods
        'no-sync': 2,

        // Stylistic Issues
        //  These rules relate to style guidelines, and are therefore quite subjective:

        // enforce consistent spacing inside array brackets
        'array-bracket-spacing': [2, 'always'],
        // enforce consistent spacing inside single-line blocks
        'block-spacing': [2, 'always'],
        // enforce consistent brace style for blocks
        'brace-style': [2, '1tbs'],
        // enforce camelcase naming convention
        'camelcase': [2, {'properties': 'never'}],
        // enforce or disallow capitalization of the first letter of a comment
        'capitalized-comments': 0,
        // require or disallow trailing commas
        'comma-dangle': [2, 'always-multiline'],
        // enforce consistent spacing before and after commas
        'comma-spacing': [2, {'before': false, 'after': true}],
        // enforce consistent comma style
        'comma-style': [2, 'last'],
        // enforce consistent spacing inside computed property brackets
        'computed-property-spacing': [2, 'always'],
        // enforce consistent naming when capturing the current execution context
        'consistent-this': [2, 'that', '$$'],
        // require or disallow newline at the end of files
        'eol-last': [2, 'always'],
        // require or disallow spacing between function identifiers and their invocations
        'func-call-spacing': [2, 'never'],
        // require function names to match the name of the variable or property to which they are assigned
        'func-name-matching': 0,
        // require or disallow named function expressions
        'func-names': [2, 'never'],
        // enforce the consistent use of either function declarations or expressions
        'func-style': [2, 'declaration', {'allowArrowFunctions': true}],
        // disallow specified identifiers
        'id-blacklist': 0,
        // enforce minimum and maximum identifier lengths
        'id-length': 0,
        // require identifiers to match a specified regular expression
        'id-match': 0,
        // enforce consistent indentation
        'indent': [2, 4, {
            'SwitchCase': 1
        }],
        // enforce the consistent use of either double or single quotes in JSX attributes
        'jsx-quotes': [2, 'prefer-double'],
        // enforce consistent spacing between keys and values in object literal properties
        'key-spacing': [2, {
            'beforeColon': false,
            'afterColon': true
        }],
        // enforce consistent spacing before and after keywords
        'keyword-spacing': 2,
        // enforce position of line comments
        'line-comment-position': 0,
        // enforce consistent linebreak style
        'linebreak-style': [0, 'windows'],
        // require empty lines around comments
        'lines-around-comment': 0,
        // require or disallow newlines around directives
        'lines-around-directive': 0,
        // enforce a maximum depth that blocks can be nested
        'max-depth': [2, 4],
        // enforce a maximum line length
        'max-len': [2, 160],
        // enforce a maximum number of lines per file
        'max-lines': 0,
        // enforce a maximum depth that callbacks can be nested
        'max-nested-callbacks': 0,
        // enforce a maximum number of parameters in function definitions
        'max-params': [2, 10],
        // enforce a maximum number of statements allowed in function blocks
        'max-statements': [2, 42],
        // enforce a maximum number of statements allowed per line
        'max-statements-per-line': [2, {'max': 1}],
        // enforce newlines between operands of ternary expressions
        'multiline-ternary': 0,
        // require constructor names to begin with a capital letter
        'new-cap': 2,
        // require parentheses when invoking a constructor with no arguments
        'new-parens': 2,
        // require or disallow an empty line after variable declarations
        'newline-after-var': 0,
        // require an empty line before return statements
        'newline-before-return': 0,
        // require a newline after each call in a method chain
        'newline-per-chained-call': 0,
        // disallow Array constructors
        'no-array-constructor': 2,
        // disallow bitwise operators
        'no-bitwise': 0,
        // disallow continue statements
        'no-continue': 0,
        // disallow inline comments after code
        'no-inline-comments': 0,
        // disallow if statements as the only statement in else blocks
        'no-lonely-if': 2,
        // disallow mixed binary operators
        'no-mixed-operators': 0,
        // disallow mixed spaces and tabs for indentation
        'no-mixed-spaces-and-tabs': 2,
        // disallow use of chained assignment expressions
        'no-multi-assign': 0,
        // disallow multiple empty lines
        'no-multiple-empty-lines': [2, {max: 2}],
        // disallow negated conditions
        'no-negated-condition': 0,
        // disallow nested ternary expressions
        'no-nested-ternary': 0,
        // disallow Object constructors
        'no-new-object': 2,
        // disallow the unary operators ++ and --
        'no-plusplus': 0,
        // disallow specified syntax
        'no-restricted-syntax': 0,
        // disallow all tabs
        'no-tabs': 2,
        // disallow ternary operators
        'no-ternary': 0,
        // disallow trailing whitespace at the end of lines
        'no-trailing-spaces': 2,
        // disallow dangling underscores in identifiers
        'no-underscore-dangle': 0,
        // disallow ternary operators when simpler alternatives exist
        'no-unneeded-ternary': 2,
        // disallow whitespace before properties
        'no-whitespace-before-property': 2,
        // enforce the location of single-line statements
        'nonblock-statement-body-position': 0,
        // enforce consistent line breaks inside braces
        'object-curly-newline': 0,
        // enforce consistent spacing inside braces
        'object-curly-spacing': [2, 'always'],
        // enforce placing object properties on separate lines
        'object-property-newline': 0,
        // enforce variables to be declared either together or separately in functions
        'one-var': [2, {'uninitialized': 'always', 'initialized': 'never'}],
        // require or disallow newlines around variable declarations
        'one-var-declaration-per-line': 0,
        // require or disallow assignment operator shorthand where possible
        'operator-assignment': [2, 'always'],
        // enforce consistent linebreak style for operators
        'operator-linebreak': [0, 'before'],
        // require or disallow padding within blocks
        'padded-blocks': 0,
        // require quotes around object literal property names
        'quote-props': 0,
        // enforce the consistent use of either backticks, double, or single quotes
        'quotes': [2, 'double', {'allowTemplateLiterals' : true}],
        // require JSDoc comments
        'require-jsdoc': 0,
        // require or disallow semicolons instead of ASI
        'semi': [2, 'always'],
        // enforce consistent spacing before and after semicolons
        'semi-spacing': 2,
        // require object keys to be sorted
        'sort-keys': 0,
        // require variables within the same declaration block to be sorted
        'sort-vars': 0,
        // enforce consistent spacing before blocks
        'space-before-blocks': 0,
        // enforce consistent spacing before function definition opening parenthesis
        'space-before-function-paren': [2, 'never'],
        // enforce consistent spacing inside parentheses
        'space-in-parens': [0, 'always', {'exceptions': ['{}', '[]', '()', 'empty']}],
        // require spacing around infix operators
        'space-infix-ops': [2, {'int32Hint': true}],
        // enforce consistent spacing before or after unary operators
        'space-unary-ops': [2, {'words': true, 'nonwords': false}],
        // enforce consistent spacing after the // or /* in a comment
        'spaced-comment': 0,
        // require or disallow spacing between template tags and their literals
        'template-tag-spacing': 0,
        // require or disallow Unicode byte order mark (BOM)
        'unicode-bom': [2, 'never'],
        // require parenthesis around regex literals
        'wrap-regex': [2],

        // ECMAScript 6
        // These rules relate to ES6, also known as ES2015:

        // require braces around arrow function bodies
        'arrow-body-style': [0, 'as-needed'],
        // require parentheses around arrow function arguments
        'arrow-parens': [2, 'as-needed'],
        // enforce consistent spacing before and after the arrow in arrow functions
        'arrow-spacing': 2,
        // require super() calls in constructors
        'constructor-super': 2,
        // enforce consistent spacing around * operators in generator functions
        'generator-star-spacing': 0,
        // disallow reassigning class members
        'no-class-assign': 2,
        // disallow arrow functions where they could be confused with comparisons
        'no-confusing-arrow': 0,
        // disallow reassigning const variables
        'no-const-assign': 2,
        // disallow duplicate class members
        'no-dupe-class-members': 2,
        // disallow duplicate module imports
        'no-duplicate-imports': 2,
        // disallow new operators with the Symbol object
        'no-new-symbol': 2,
        // disallow specified modules when loaded by import
        'no-restricted-imports': 0,
        // disallow this/super before calling super() in constructors
        'no-this-before-super': 2,
        // disallow unnecessary computed property keys in object literals
        'no-useless-computed-key': 2,
        // disallow unnecessary constructors
        'no-useless-constructor': 2,
        // disallow renaming import, export, and destructured assignments to the same name
        'no-useless-rename': 2,
        // require let or const instead of var
        'no-var': 2,
        // require or disallow method and property shorthand syntax for object literals
        'object-shorthand': 2,
        // require arrow functions as callbacks
        'prefer-arrow-callback': 2,
        // require const declarations for variables that are never reassigned after declared
        'prefer-const': 2,
        // require destructuring from arrays and/or objects
        'prefer-destructuring': 2,
        // disallow parseInt() in favor of binary, octal, and hexadecimal literals
        'prefer-numeric-literals': 2,
        // require rest parameters instead of arguments
        'prefer-rest-params': 0,
        // require spread operators instead of .apply()
        'prefer-spread': 2,
        // require template literals instead of string concatenation
        'prefer-template': 0,
        // require generator functions to contain yield
        'require-yield': 0,
        // enforce spacing between rest and spread operators and their expressions
        'rest-spread-spacing': [2, 'never'],
        // enforce sorted import declarations within modules
        'sort-imports': 0,
        // require symbol descriptions
        'symbol-description': 2,
        // require or disallow spacing around embedded expressions of template strings
        'template-curly-spacing': 0,
        // require or disallow spacing around the * in yield* expressions
        'yield-star-spacing': 0,

        // REACT

        // List of supported rules

        // Prevent missing displayName in a React component definition
        'react/display-name': 0,
        // Forbid certain props on Components
        'react/forbid-component-props': 0,
        // Forbid certain elements
        'react/forbid-elements': 0,
        // Forbid certain propTypes
        'react/forbid-prop-types': 0,
        // Forbid foreign propTypes
        'react/forbid-foreign-prop-types': 0,
        // Prevent using Array index in key props
        'react/no-array-index-key': 2,
        // Prevent passing children as props
        'react/no-children-prop': 0,
        // Prevent usage of dangerous JSX properties
        'react/no-danger': 2,
        // Prevent problem with children and props.dangerouslySetInnerHTML
        'react/no-danger-with-children': 2,
        // Prevent usage of deprecated methods
        'react/no-deprecated': 2,
        // Prevent usage of setState in componentDidMount
        'react/no-did-mount-set-state': 2,
        // Prevent usage of setState in componentDidUpdate
        'react/no-did-update-set-state': 2,
        // Prevent direct mutation of this.state
        'react/no-direct-mutation-state': 1,
        // Prevent usage of findDOMNode
        'react/no-find-dom-node': 2,
        // Prevent usage of isMounted
        'react/no-is-mounted': 2,
        // Prevent multiple component definition per file
        'react/no-multi-comp': 0,
        // Prevent usage of the return value of React.render
        'react/no-render-return-value': 2,
        // Prevent usage of setState
        'react/no-set-state': 0,
        // Prevent using string references in ref attribute.
        'react/no-string-refs': 2,
        // Prevent invalid characters from appearing in markup
        'react/no-unescaped-entities': 2,
        // Prevent usage of unknown DOM property (fixable)
        'react/no-unknown-property': 2,
        // Prevent definitions of unused prop types
        'react/no-unused-prop-types': 2,
        // Enforce ES5 or ES6 class for React Components
        'react/prefer-es6-class': 2,
        // Enforce stateless React Components to be written as a pure function
        'react/prefer-stateless-function': 0,
        // Prevent missing props validation in a React component definition
        'react/prop-types': 1,
        // Prevent missing React when using JSX
        'react/react-in-jsx-scope': 2,
        // Enforce a defaultProps definition for every prop that is not a required prop
        'react/require-default-props': 0,
        // Enforce React components to have a shouldComponentUpdate method
        'react/require-optimization': 0,
        // Enforce ES5 or ES6 class for returning value in render function
        'react/require-render-return': 2,
        // Prevent extra closing tags for components without children (fixable)
        'react/self-closing-comp': 2,
        // Enforce component methods order
        'react/sort-comp': 0,
        // Enforce propTypes declarations alphabetical sorting
        'react/sort-prop-types': 0,
        // Enforce style prop value being an object
        'react/style-prop-object': 2,
        // Prevent void DOM elements (e.g. <img />, <br />) from receiving children
        'react/void-dom-elements-no-children': 2,

        // JSX-specific rules

        // Enforce boolean attributes notation in JSX (fixable)
        'react/jsx-boolean-value': 0,
        // Validate closing bracket location in JSX (fixable)
        'react/jsx-closing-bracket-location': 0,
        // Enforce or disallow spaces inside of curly braces in JSX attributes (fixable)
        'react/jsx-curly-spacing': [2, 'always', {'spacing': {'objectLiterals': 'never'}}],
        // Enforce or disallow spaces around equal signs in JSX attributes (fixable)
        'react/jsx-equals-spacing': [2, 'never'],
        // Restrict file extensions that may contain JSX
        'react/jsx-filename-extension': 0,
        // Enforce position of the first prop in JSX (fixable)
        'react/jsx-first-prop-new-line': 0,
        // Enforce event handler naming conventions in JSX
        'react/jsx-handler-names': 0,
        // Validate JSX indentation (fixable)
        'react/jsx-indent': [2, 4],
        // Validate props indentation in JSX (fixable)
        'react/jsx-indent-props': 0,
        // Validate JSX has key prop when in array or iterator
        'react/jsx-key': 2,
        // Limit maximum of props on a single line in JSX
        'react/jsx-max-props-per-line': 0,
        // Prevent usage of .bind() and arrow functions in JSX props
        'react/jsx-no-bind': 0,
        // Prevent comments from being inserted as text nodes
        'react/jsx-no-comment-textnodes': 0,
        // Prevent duplicate props in JSX
        'react/jsx-no-duplicate-props': 2,
        // Prevent usage of unwrapped JSX strings
        'react/jsx-no-literals': 0,
        // Prevent usage of unsafe target='_blank'
        'react/jsx-no-target-blank': 2,
        // Disallow undeclared variables in JSX
        'react/jsx-no-undef': 2,
        // Enforce PascalCase for user-defined JSX components
        'react/jsx-pascal-case': 2,
        // Enforce props alphabetical sorting
        'react/jsx-sort-props': 0,
        // Validate spacing before closing bracket in JSX (fixable)
        'react/jsx-space-before-closing': 0,
        // Validate whitespace in and around the JSX opening and closing brackets (fixable)
        'react/jsx-tag-spacing': 0,
        // Prevent React to be incorrectly marked as unused
        'react/jsx-uses-react': 2,
        // Prevent variables used in JSX to be incorrectly marked as unused
        'react/jsx-uses-vars': 2,
        // Prevent missing parentheses around multilines JSX (fixable)
        'react/jsx-wrap-multilines': 0,

    }
};
