import React from "react";
import htmlParser from "html-react-parser";

import styles from "./index.less";

import changelog from "../../../../changelog.md";

export default class Desktop extends React.Component{

    render(){

        return <div>
            <section className={ styles.changelog }>
                { htmlParser(changelog) }
            </section>
        </div>;
    }
}
