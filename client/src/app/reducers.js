import { combineReducers } from "redux";

import apiTest from "./ApiTest/reducers.js";

export default combineReducers({
    apiTest,
});

