import React from "react";

import RequestList from "./RequestList";
import RequestEdit from "./RequestEdit";
import BodyEdit from "./BodyEdit";
import RequestButton from "./RequestButton";
import ResponseView from "./ResponseView";

import requests from "./requests";

export default class ApiTest extends React.Component{

    constructor(props) {
        super(props);

        this.state = { ...requests[ 0 ].items[ 0 ] };

        this.handleListChange = this.handleListChange.bind(this);
        this.handleRequestChange = this.handleRequestChange.bind(this);
        this.handleBodyChange = this.handleBodyChange.bind(this);
    }

    handleListChange(v){

        this.setState(v);
    }

    handleRequestChange(request){

        this.setState({ request });
    }

    handleBodyChange(body){

        this.setState({ body });
    }

    renderInput(){

        return <input onChange={ this.handleChange }/>;
    }

    render(){

        const { method, request, body } = this.state;

        return <div>
            <RequestList onChange={ this.handleListChange }/>
            <RequestEdit label={ `Request   [${method}]` } value={ request } onChange={ this.handleRequestChange }/>
            { method === "POST" ? <BodyEdit label="Body" value={ body } onChange={ this.handleBodyChange }/> : null }
            <RequestButton value={{ method, request, body }}/>
            <ResponseView method={ method }/>
        </div>;
    }
}
