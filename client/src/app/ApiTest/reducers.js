import { actionTypes } from "./actions";

export default (state = {}, action) => {

    switch (action.type) {
        case actionTypes.GET_REQUEST_COMPLETE: {
            const { response } = action;
            return { ...state, response };
        }
        default:
            return state;
    }
};
