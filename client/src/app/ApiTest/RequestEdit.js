import React from "react";
import PropTypes from "prop-types";

import styles from "./index.less";

export default class RequestEdit extends React.Component{

    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(e){
        this.props.onChange(e.target.value);
    }

    render(){

        const { label, value = "" } = this.props;

        return <div className={ styles.field }>
            <label>{label}</label>
            <input type="text" value={ value } onChange={ this.handleChange }/>
        </div>;
    }
}

RequestEdit.propTypes = {
    label: PropTypes.string,
    value: PropTypes.string,
    onChange: PropTypes.func.isRequired,
};
