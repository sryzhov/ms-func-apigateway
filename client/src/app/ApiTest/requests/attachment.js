export default [

    { id: "attachment-post",
        name: "Загрузить",
        method: "POST",
        request: "/api/web/v0.1/attachments",
        body: JSON.stringify({
            "parentType": "document",
            "parentId": "000-111",
            "filename": "тестовый файл-3.docx",
            "content": "SGVbiG8gd29ybGQK",
        }, null, 2) },

    { id: "attachment-get",
        name: "Список вложений",
        method: "GET",
        request: "/api/web/v0.1/attachments?parentType=document&parentId=000-111&limit=100" },

    { id: "attachment-get-by-id",
        name: "Вложение по id",
        method: "GET",
        request: "/api/web/v0.1/attachments/00000000-0000-0000-0000-000000000000?parentType=document&parentId=000-111" },

    { id: "attachment-update",
        name: "Изменить вложение",
        method: "POST",
        request: "/api/web/v0.1/attachments/00000000-0000-0000-0000-000000000000",
        body: JSON.stringify({
            "parentType": "document",
            "parentId": "000-111",
            "filename": "тестовый файл-3.docx",
            "content": "SGVbiG8gd29ybGQK",
        }, null, 2) },

    { id: "attachment-delete",
        name: "Удалить вложение",
        method: "DELETE",
        request: "/api/web/v0.1/attachments/00000000-0000-0000-0000-000000000000",
    },

    { id: "attachment-open-editor",
        name: "Ссылка на редактор",
        method: "GET",
        request: "/api/web/v0.1/attachments/00000000-0000-0000-0000-000000000000/editor?parentType=document&parentId=000-111&majorVersion=1&minorVersion=1",
    },

    { id: "attachment-modify-position",
        name: "Изменить порядок вложений",
        method: "POST",
        request: "/api/web/v0.1/attachments/modifyPosition/",
        body: JSON.stringify({
            "parentType": "document",
            "parentId": "000-111",
            "identsOrder": [
                "00000000-0000-0000-0000-000000000000",
                "11111111-1111-1111-1111-111111111111",
            ],
        }, null, 2) },

    { id: "attachment-rename",
        name: "Переименовать вложение",
        method: "POST",
        request: "/api/web/v0.1/attachments/00000000-0000-0000-0000-000000000000/rename/",
        body: JSON.stringify({
            "parentType": "document",
            "parentId": "000-111",
            "renameTo": "тестовый файл-5555",
        }, null, 2) },

    { id: "attachment-capacity",
        name: "Capacity",
        method: "GET",
        request: "/api/web/v0.1/attachments/capacity?parentType=document&parentId=000-111",
    },
];
