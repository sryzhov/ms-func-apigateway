export default [

    { id: "employee-search",
        name: "Поиск",
        method: "GET",
        request: "/api/web/v0.1/employees?q=ван&type=sender",
    },

    { id: "employee-by-id",
        name: "Сотрудник по id",
        method: "GET",
        request: "/api/web/v0.1/employees/1740572?variant=full",
    },
];
