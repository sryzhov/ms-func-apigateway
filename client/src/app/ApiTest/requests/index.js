import document from "./document";
import attachment from "./attachment";
import employee from "./employee";
import comment from "./comment";
import reference from "./reference";

export default [
    { name: "Документ", items: document },
    { name: "Вложение", items: attachment },
    { name: "Сотрудник", items: employee },
    { name: "Комментарий", items: comment },
    { name: "Справочники", items: reference },
];
