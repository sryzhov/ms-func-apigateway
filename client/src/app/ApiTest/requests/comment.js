export default [

    { id: "comment-add",
        name: "Добавить",
        method: "POST",
        request: "api/web/v0.1/comments/document?entityId=00000000-0000-0000-0000-000000000000&parentId=11111111-1111-1111-1111-111111111111",
        body: JSON.stringify({
            content: "some comment",
            author: { "id": "22222222-2222-2222-2222-222222222222",
                name: "Иванов И.И.",
                post: "аналитик",
            },
        }, null, 2) },
    { id: "comment-edit",
        name: "Изменить",
        method: "POST",
        request: "api/web/v0.1/comments/document/33333333-3333-3333-3333-333333333333?entityId=00000000-0000-0000-0000-000000000000&parentId=",
        body: JSON.stringify({
            content: "edited comment",
            author: { "id": "22222222-2222-2222-2222-222222222222",
                name: "Иванов И.И.",
                post: "аналитик",
            },
        }, null, 2) },
    { id: "comment-delete",
        name: "Удалить",
        method: "DELETE",
        request: "api/web/v0.1/comments/document/00000000-0000-0000-0000-000000000000?entityId=00000000-0000-0000-0000-000000000000&parentId=" },
    { id: "comment-tree",
        name: "Дерево",
        method: "GET",
        request: "api/web/v0.1/comments/document/tree?entityId=00000000-0000-0000-0000-000000000000&parentId=&depth=0" },
    { id: "comment-response",
        name: "Отправить ответ",
        method: "POST",
        request: "api/web/v0.1/comments/document/33333333-3333-3333-3333-333333333333/response?entityId=00000000-0000-0000-0000-000000000000&parentId=",
        body: JSON.stringify({
            content: "ok!",
            author: {
                id: "22222222-2222-2222-2222-222222222222",
            },
        }, null, 2) },
    { id: "comment-attachment-add",
        name: "Добавить вложение",
        method: "POST",
        request: "api/web/v0.1/comments/document/33333333-3333-3333-3333-333333333333/attachment",
        body: JSON.stringify({
            "attachmentId": "9aea760e-441a-11eb-b378-0242ac130004",
            "filename": "табличЬка_номер_одиннадцать.xlsx",
            "position": "0",
        }, null, 2) },
    { id: "comment-attachment-delete",
        name: "Удалить вложение",
        method: "DELETE",
        request: "api/web/v0.1/comments/document/33333333-3333-3333-3333-333333333333/attachment/00000000-0000-0000-0000-000000000000" },
];
