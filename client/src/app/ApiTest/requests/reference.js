export default [

    { id: "reference-get-all",
        name: "getAll",
        method: "GET",
        request: "/api/web/v0.1/reference",
    },
];
