import * as serverActions from "src/core/server/actions";

const headers = {
    "Content-Type": "application/json",
    "transactionId": "00000000-0000-0000-0000-000000000000",
    "userId": "1740572",
};

export const actionTypes = {
    GET_REQUEST_COMPLETE: "apiTestController/getRequestComplete",
};

export const executeRequest = ({ request, body, method }) => {

    const params = method === "POST" ? { headers, body, method } : { headers, method };
    return serverActions.executeRequest(request, params);
};

export const getRequestComplete = response => ({
    type: actionTypes.GET_REQUEST_COMPLETE,
    response,
});
