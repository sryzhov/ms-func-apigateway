import React from "react";
import PropTypes from "prop-types";

import requests from "./requests";

import styles from "./index.less";

export default class RequestList extends React.Component{

    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
    }


    handleChange(e){

        const id = e.target.value;
        const item = requests.flatMap(r => r.items).find(i => i.id === id);

        this.props.onChange(item);
    }

    static renderGroups(){

        return requests.map(r => RequestList.renderGroup(r));
    }

    static renderGroup(g){

        return <optgroup key={ g.name }label={ g.name }>
            {g.items.map(item => RequestList.renderItem( item))}
        </optgroup>;
    }

    static renderItem(item){

        const { id, name } = item;

        return <option key={ name } value={ id } >{name}</option>;
    }

    render(){

        return <div>
            <select className={ styles.select } onChange={ this.handleChange }>
                {RequestList.renderGroups()}
            </select>
        </div>;
    }
}

RequestList.propTypes = {
    onChange: PropTypes.func.isRequired,
};
