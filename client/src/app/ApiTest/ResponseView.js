import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";

import * as selectors from "./selectors";

import styles from "./index.less";

class ResponseView extends React.Component{

    render(){

        const { response, method } = this.props;

        const style = method === "POST" ? styles.postResponse : styles.response;

        return <div>
            <textarea className={ style } value={ response }/>
        </div>;
    }
}

ResponseView.propTypes = {
    method: PropTypes.string,
    response: PropTypes.string,
};

const mapStateToProps = state => {
    return {
        response: selectors.responseSelector(state),
    };
};


export default connect(mapStateToProps)(ResponseView);
