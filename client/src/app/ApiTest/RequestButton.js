import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";

import * as actions from "./actions";

import styles from "./index.less";

class RequestButton extends React.Component{

    constructor(props) {
        super(props);

        this.handleClick = this.handleClick.bind(this);
    }

    handleClick(){

        const { value, executeRequest } = this.props;

        executeRequest(value);
    }

    render(){

        return <div>
            <button className={ styles.button } onClick={ this.handleClick } >Execute</button>
        </div>;
    }
}

RequestButton.propTypes = {
    value: PropTypes.object,
    executeRequest: PropTypes.func.isRequired,
};

const mapDispatchToProps = dispatch => {
    return {
        executeRequest: value => actions.executeRequest(value)
            .then(response => dispatch(actions.getRequestComplete(response))),
    };
};

export default connect(null, mapDispatchToProps)(RequestButton);
