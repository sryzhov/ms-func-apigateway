const API_TEST = { path: "/api-test" };
const CHANGELOG = { path: "/changelog" };

export default { API_TEST, CHANGELOG };
