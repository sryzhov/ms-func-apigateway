import { routerReducer } from "react-router-redux";
import { combineReducers } from "redux";

import app from "./app/reducers.js";

export default combineReducers({
    routing: routerReducer,
    app,
});

