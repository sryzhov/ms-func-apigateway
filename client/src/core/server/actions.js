export const executeRequest = (url, params) => {

    const request = { url, params };

    const promise = new Promise( (resolve, reject) => {
        request.resolve = resolve;
        request.reject = reject;
    });
    fetch(url, params)
        .then( r => r.json().then(json => request.resolve(json)))
        .catch(e => request.reject(e))
        .catch(e => request.reject(e));

    return promise;
};

export const get = (url, params) => executeRequest(url(params), { method: "GET" });
