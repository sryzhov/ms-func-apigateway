import React from "react";

import routes from "./routes";
import styles from "./index.less";

export default class Desktop extends React.Component{

    render(){

        return <div>
            <div className={ styles.menu }><a href="/swagger-ui.html">Swagger API</a></div>
            <div className={ styles.menu }><a href={ `#${routes.API_TEST.path}` } >API test</a></div>
            <div className={ styles.menu }><a href={ `#${routes.CHANGELOG.path}` } >Changelog</a></div>
        </div>;
    }
}
