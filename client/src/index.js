import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { Route } from "react-router";
import { HashRouter } from "react-router-dom";

import configureStore from "./configureStore";
import Desktop from "./Desktop";
import ApiTest from "./app/ApiTest";
import Changelog from "./app/Changelog";
import routes from "./routes";

import styles from "./index.less";

const store = configureStore();

class App extends React.Component{

    render(){

        return <Provider store={ store }>
            <div className={ styles.app }>
                <HashRouter>
                    <Route exact path="/" component={ Desktop }/>
                    <Route path={ "" }>
                        <Route title="Test Api" path={ routes.API_TEST.path } component={ ApiTest }/>
                        <Route title="Changelog" path={ routes.CHANGELOG.path } component={ Changelog }/>
                    </Route>
                </HashRouter>
            </div>
        </Provider>;
    }
}


ReactDOM.render( <App/>, document.getElementById("app"));
