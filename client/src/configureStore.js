import { applyMiddleware, createStore } from "redux";
import thunkMiddleware from "redux-thunk";
import { composeWithDevTools } from "redux-devtools-extension";

import reducer from "./reducers";

const middleware = [
    thunkMiddleware,
];

const defaultApplyMiddleware = applyMiddleware(...middleware);
const enhancer = composeWithDevTools(defaultApplyMiddleware);

export default initialState => createStore(reducer, initialState, enhancer);

