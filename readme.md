##### В свойствах надо задать:

`cseodo.service.document.name` - имя сервиса "Документ", по умолчанию "document"  
`cseodo.service.document.url` - URL, если не используется service discovery. Например, http://localhost:8084/SberbankCSEODO/CSEODO/1/document.  
`cseodo.service.numeration.name` - имя сервиса нумерации документов, по умолчанию "numeration"  
`cseodo.service.numeration.url` - URL, если не используется service discovery. Например, http://localhost:8080/web/v1/services/sysNumber.  
`cseodo.service.employee.name` - имя сервиса "Сотрудники", по умолчанию "employee"  
`cseodo.service.employee.url`  
`cseodo.service.attachment.name` - имя сервиса "Вложения", по умолчанию "attachment"  
`cseodo.service.attachment.url`   
  
`cseodo.service.gateway.user.read` - тестовый пользователь с правами на чтение. _Optional_    
`cseodo.service.gateway.user.write` - тестовый пользователь с правами на запись. _Optional_       

##### API:
/swagger-ui.html  
/v3/api-docs                           
