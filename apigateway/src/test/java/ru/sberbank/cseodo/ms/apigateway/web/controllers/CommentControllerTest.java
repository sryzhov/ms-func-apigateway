package ru.sberbank.cseodo.ms.apigateway.web.controllers;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import ru.sberbank.cseodo.ms.apigateway.feign.CommentApiClient;
import ru.sberbank.cseodo.ms.apigateway.service.CommentService;

import java.util.Collections;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = {CommentController.class,
        CommentService.class})
@MockBean(classes = {CommentApiClient.class})
public class CommentControllerTest {

    public static final String COMMENT_RESPONSE = "Here must be some reference";

    @Autowired
    CommentController controller;

    @MockBean
    CommentApiClient apiClient;

    @Test
    public void getTreeComments() {

        Mockito.when(apiClient.getTreeComments(
                Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any()))
                .thenReturn(COMMENT_RESPONSE);

        Object response = controller.getTreeComments(
                null, null, null, null, null);

        Assert.assertEquals(COMMENT_RESPONSE, response);
    }

    @Test
    public void addComment() {

        Mockito.when(apiClient.addComment(
                Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any()))
                .thenReturn(COMMENT_RESPONSE);

        Object response = controller.addComment(
                null, null, null, null, Collections.emptyMap());

        Assert.assertEquals(COMMENT_RESPONSE, response);
    }

    @Test
    public void editComment() {

        Mockito.when(apiClient.editComment(
                Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any()))
                .thenReturn(COMMENT_RESPONSE);

        Object response = controller.editComment(
                null, null, null, null, null, Collections.emptyMap());

        Assert.assertEquals(COMMENT_RESPONSE, response);
    }

    @Test
    public void removeComment() {

        Mockito.when(apiClient.removeComment(
                Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any()))
                .thenReturn(COMMENT_RESPONSE);

        Object response = controller.removeComment(
                null, null, null, null, null);

        Assert.assertEquals(COMMENT_RESPONSE, response);
    }

    @Test
    public void responseComment() {

        Mockito.when(apiClient.responseComment(
                Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any()))
                .thenReturn(COMMENT_RESPONSE);

        Object response = controller.responseComment(
                null, null, null, null, null, Collections.emptyMap());

        Assert.assertEquals(COMMENT_RESPONSE, response);
    }

    @Test
    public void addAttachment() {

        Mockito.when(apiClient.addAttachment(
                Mockito.any(), Mockito.any(), Mockito.any()))
                .thenReturn(COMMENT_RESPONSE);

        Object response = controller.addAttachment(
                null, null, null, Collections.emptyMap());

        Assert.assertEquals(COMMENT_RESPONSE, response);
    }

    @Test
    public void removeAttachment() {

        Mockito.when(apiClient.removeAttachment(
                Mockito.any(), Mockito.any()))
                .thenReturn(COMMENT_RESPONSE);

        Object response = controller.removeAttachment(
                null, null, null, null);

        Assert.assertEquals(COMMENT_RESPONSE, response);
    }
}
