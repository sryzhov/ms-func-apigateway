package ru.sberbank.cseodo.ms.apigateway.web.controllers;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import ru.sberbank.cseodo.ms.apigateway.dto.response.GatewayResponse;
import ru.sberbank.cseodo.ms.apigateway.dto.response.Response;
import ru.sberbank.cseodo.ms.apigateway.feign.EmployeeApiClient;
import ru.sberbank.cseodo.ms.apigateway.service.EmployeeService;

import java.util.HashMap;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = {EmployeeController.class,
        EmployeeService.class})
@MockBean(classes = {EmployeeApiClient.class})
public class EmployeeControllerTest {

    public static final String EMPLOYEE_OBJECT = "Here must be employee object";
    private static final String SOMETHING_WRONG = "something wrong";

    @Autowired
    EmployeeController controller;

    @MockBean
    EmployeeApiClient employeeApiClient;

    @Test
    public void searchRecipient() {

        Mockito.when(employeeApiClient.searchForAddressees(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any())).thenAnswer(
                invocation -> {
                    Response<String> response = new Response<>();
                    response.setSuccess(true);
                    response.setData(EMPLOYEE_OBJECT);
                    return response;
                });
        Mockito.when(employeeApiClient.searchForSenders(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any())).thenAnswer(
                invocation -> {
                    Response<String> response = new Response<>();
                    response.setSuccess(true);
                    response.setData(SOMETHING_WRONG);
                    return response;
                });

        GatewayResponse response = controller.search("", "recipient", new HashMap<>());

        Assert.assertTrue(response instanceof Response);
        Assert.assertEquals(EMPLOYEE_OBJECT, ((Response<String>)response).getData());
    }

    @Test
    public void searchSender() {

        Mockito.when(employeeApiClient.searchForAddressees(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any())).thenAnswer(
                invocation -> {
                    Response<String> response = new Response<>();
                    response.setSuccess(true);
                    response.setData(SOMETHING_WRONG);
                    return response;
                });
        Mockito.when(employeeApiClient.searchForSenders(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any())).thenAnswer(
                invocation -> {
                    Response<String> response = new Response<>();
                    response.setSuccess(true);
                    response.setData(EMPLOYEE_OBJECT);
                    return response;
                });

        GatewayResponse response = controller.search("", "sender", new HashMap<>());

        Assert.assertTrue(response instanceof Response);
        Assert.assertEquals(EMPLOYEE_OBJECT, ((Response) response).getData());
    }

    @Test
    public void searchBadType() {

        GatewayResponse response = controller.search("", "bad type", new HashMap<>());

        Assert.assertTrue(response instanceof Response);
        Assert.assertEquals("400", ((Response) response).getError().getCode());
        Assert.assertEquals("bad type - неверный тип поиска сотрудника. Допустимые значения: recipient, sender",
                ((Response) response).getError().getMessage());
    }


    @Test
    public void get() {

        Mockito.when(employeeApiClient.search(Mockito.any(), Mockito.any(), Mockito.any())).thenAnswer(
                invocation -> {
                    Response<String> response = new Response<>();
                    response.setSuccess(true);
                    response.setData(EMPLOYEE_OBJECT);
                    return response;
                });

        Response<Object> response = controller.get("1", "full", new HashMap<>());
        Object data = response.getData();

        Assert.assertTrue(data instanceof String);
        Assert.assertEquals(EMPLOYEE_OBJECT, data);
    }
}
