package ru.sberbank.cseodo.ms.apigateway.web.controllers;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import ru.sberbank.cseodo.ms.apigateway.dto.document.BackDocumentDto;
import ru.sberbank.cseodo.ms.apigateway.dto.document.BackDocumentItemDto;
import ru.sberbank.cseodo.ms.apigateway.dto.document.FrontDocumentDto;
import ru.sberbank.cseodo.ms.apigateway.dto.document.FrontDocumentRouteDto;
import ru.sberbank.cseodo.ms.apigateway.dto.numeration.NumerationResult;
import ru.sberbank.cseodo.ms.apigateway.dto.response.ErrorDto;
import ru.sberbank.cseodo.ms.apigateway.dto.response.Response;
import ru.sberbank.cseodo.ms.apigateway.dto.route.RouteDataAuthorDto;
import ru.sberbank.cseodo.ms.apigateway.dto.route.RouteDocumentDto;
import ru.sberbank.cseodo.ms.apigateway.dto.route.RouteDocumentVersionDto;
import ru.sberbank.cseodo.ms.apigateway.dto.route.RouteDto;
import ru.sberbank.cseodo.ms.apigateway.feign.DocumentApiClient;
import ru.sberbank.cseodo.ms.apigateway.feign.EmployeeApiClient;
import ru.sberbank.cseodo.ms.apigateway.feign.NumerationApiClient;
import ru.sberbank.cseodo.ms.apigateway.feign.RouteApiClient;
import ru.sberbank.cseodo.ms.apigateway.mappers.DictionaryConverter;
import ru.sberbank.cseodo.ms.apigateway.mappers.DocumentItemFullMapperImpl;
import ru.sberbank.cseodo.ms.apigateway.mappers.DocumentItemMapperImpl;
import ru.sberbank.cseodo.ms.apigateway.mappers.DocumentMapperImpl;
import ru.sberbank.cseodo.ms.apigateway.service.DocumentService;
import ru.sberbank.cseodo.ms.apigateway.service.NumerationService;
import ru.sberbank.cseodo.ms.apigateway.service.RouteService;

import java.time.LocalDate;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static ru.sberbank.cseodo.ms.apigateway.web.controllers.EmployeeControllerTest.EMPLOYEE_OBJECT;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = {DocumentController.class,
        NumerationService.class,
        DocumentService.class,
        RouteService.class,
        DocumentMapperImpl.class,
        DocumentItemMapperImpl.class,
        DocumentItemFullMapperImpl.class,
        DictionaryConverter.class})
@MockBean(classes = {EmployeeApiClient.class})
public class DocumentControllerTest {

    @Autowired
    DocumentController controller;

    @MockBean
    DocumentApiClient documentApiClient;

    @MockBean
    NumerationApiClient numerationApiClient;

    @MockBean
    RouteApiClient routeApiClient;

    @MockBean
    EmployeeApiClient employeeApiClient;

    @Test
    public void get() {

        Mockito.when(documentApiClient.get(Mockito.any(), Mockito.any())).thenAnswer(
                invocation -> {
                    BackDocumentDto backDocumentDto = new BackDocumentDto();
                    backDocumentDto.setDocumentGuid(UUID.randomUUID());
                    backDocumentDto.setDocumentNumber("555");
                    Response<BackDocumentDto> response = new Response<>();
                    response.setSuccess(true);
                    response.setData(backDocumentDto);
                    return response;
                });
        Mockito.when(routeApiClient.get(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any()))
                .thenReturn(new Response<>());

        Response<FrontDocumentRouteDto> frontDocumentDto = (Response<FrontDocumentRouteDto>) controller.get(UUID.randomUUID(), new HashMap<>());

        Assert.assertNotNull(frontDocumentDto);
        Assert.assertEquals("555", frontDocumentDto.getData().getNumber());
    }

    @Test
    public void route() {

        Mockito.when(employeeApiClient.search(Mockito.any(), Mockito.any(), Mockito.any())).thenAnswer(
                invocation -> {
                    Response<String> response = new Response<>();
                    response.setSuccess(true);
                    response.setData(EMPLOYEE_OBJECT);
                    return response;
                });
        Mockito.when(employeeApiClient.getSigners(Mockito.any(), Mockito.any())).thenAnswer(
                invocation -> {
                    Response<RouteDataAuthorDto> response = new Response<>();
                    response.setSuccess(true);
                    response.setData(new RouteDataAuthorDto());
                    return response;
                });
        Mockito.when(routeApiClient.create(Mockito.any(), Mockito.any(), Mockito.any())).thenAnswer(
                invocation -> {
                    RouteDocumentDto document = new RouteDocumentDto();
                    document.setVersion(new RouteDocumentVersionDto(0, 0));
                    RouteDto route = new RouteDto();
                    route.setDocument(document);
                    Response<RouteDto> response = new Response<>();
                    response.setSuccess(true);
                    response.setData(route);
                    return response;
                });
        Mockito.when(documentApiClient.get(Mockito.any(), Mockito.any())).thenAnswer(
                invocation -> {
                    Response<BackDocumentDto> response = new Response<>();
                    response.setSuccess(true);
                    response.setData(new BackDocumentDto());
                    return response;
                }
        );

        Map<String, String> headers = Collections.singletonMap("userid", "1");
        Response<RouteDataAuthorDto> response = (Response<RouteDataAuthorDto>) controller.newRoute(UUID.randomUUID(), headers);

        Assert.assertNotNull(response);
        Assert.assertEquals(EMPLOYEE_OBJECT, response.getData().getAuthor());
        Assert.assertNotNull(response.getData().getRoute());
        Assert.assertNotNull(response.getData().getRoute().getStatus());
        Assert.assertNotNull(response.getData().getRoute().getStatus());
        Assert.assertEquals("preparing", response.getData().getRoute().getStatus().getCode());
    }

    @Test
    public void routeFailUserId() {

        Response response = (Response) controller.newRoute(UUID.randomUUID(), Collections.emptyMap());

        Assert.assertNotNull(response);
        Assert.assertEquals("Не указан идентификатор пользователя", response.getError().getMessage());
    }

    @Test
    public void routeFailEmployee() {

        Mockito.when(employeeApiClient.search(Mockito.any(), Mockito.any(), Mockito.any())).thenAnswer(
                invocation -> {
                    Response<RouteDto> response = new Response<>();
                    response.setSuccess(false);
                    response.setError(new ErrorDto("499", "failed"));
                    return response;
                });

        Map<String, String> headers = Collections.singletonMap("userid", "1");
        Response response = (Response) controller.newRoute(UUID.randomUUID(), headers);

        Assert.assertNotNull(response);
        Assert.assertEquals("Ошибка получения данных сотрудника: failed", response.getError().getMessage());
    }

    @Test
    public void routeFailSigners() {

        Mockito.when(employeeApiClient.search(Mockito.any(), Mockito.any(), Mockito.any())).thenAnswer(
                invocation -> {
                    Response<String> response = new Response<>();
                    response.setSuccess(true);
                    response.setData(EMPLOYEE_OBJECT);
                    return response;
                });
        Mockito.when(employeeApiClient.getSigners(Mockito.any(), Mockito.any())).thenAnswer(
                invocation -> {
                    Response<RouteDto> response = new Response<>();
                    response.setSuccess(false);
                    response.setError(new ErrorDto("499", "failed"));
                    return response;
                });

        Map<String, String> headers = Collections.singletonMap("userid", "1");
        Response response = (Response) controller.newRoute(UUID.randomUUID(), headers);

        Assert.assertNotNull(response);
        Assert.assertEquals("Ошибка получения маршрута согласования: failed", response.getError().getMessage());
    }

    @Test
    public void routeFailCreate() {

        Mockito.when(employeeApiClient.search(Mockito.any(), Mockito.any(), Mockito.any())).thenAnswer(
                invocation -> {
                    Response<String> response = new Response<>();
                    response.setSuccess(true);
                    response.setData(EMPLOYEE_OBJECT);
                    return response;
                });
        Mockito.when(employeeApiClient.getSigners(Mockito.any(), Mockito.any())).thenAnswer(
                invocation -> {
                    Response<RouteDataAuthorDto> response = new Response<>();
                    response.setSuccess(true);
                    response.setData(new RouteDataAuthorDto());
                    return response;
                });
        Mockito.when(routeApiClient.create(Mockito.any(), Mockito.any(), Mockito.any())).thenAnswer(
                invocation -> {
                    Response<RouteDto> response = new Response<>();
                    response.setSuccess(false);
                    response.setError(new ErrorDto("499", "failed"));
                    return response;
                });

        Map<String, String> headers = Collections.singletonMap("userid", "1");
        Response response = (Response) controller.newRoute(UUID.randomUUID(), headers);

        Assert.assertNotNull(response);
        Assert.assertEquals("Ошибка сохранения маршрута согласования: failed", response.getError().getMessage());
    }

    @Test
    public void create() {

        FrontDocumentDto frontDocumentDto = new FrontDocumentDto();
        frontDocumentDto.setKind("");
        frontDocumentDto.setType("");

        Mockito.when(numerationApiClient.nextValue(Mockito.any(), Mockito.any())).thenAnswer(
                invocation -> {
                    NumerationResult numerationResult = new NumerationResult();
                    numerationResult.setSysNumber("");
                    Response<NumerationResult> response = new Response<>();
                    response.setSuccess(true);
                    response.setData(numerationResult);
                    return response;
                }
        );
        Mockito.when(documentApiClient.get(Mockito.any(), Mockito.any())).thenAnswer(
                invocation -> {
                    BackDocumentDto dto = new BackDocumentDto();
                    dto.setDocumentGuid(UUID.randomUUID());
                    dto.setCreationDate(LocalDate.now().toString());
                    dto.setItems(Collections.singletonList(new BackDocumentItemDto()));
                    dto.setPrivacy("FOR_INTERNAL_USE");
                    dto.setUrgency("URGENTLY");
                    dto.setDocumentNumber("777");
                    Response<BackDocumentDto> response = new Response<>();
                    response.setSuccess(true);
                    response.setData(dto);
                    return response;
                }
        );

        Response<FrontDocumentDto> result = controller.create(frontDocumentDto, new HashMap<>());

        Assert.assertNotNull(result);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getData().getGuid());
        Assert.assertNotNull(result.getData().getCreationDate());
        Assert.assertNotNull(result.getData().getItems());
        Assert.assertEquals("777", result.getData().getNumber());
        Assert.assertEquals("FOR_INTERNAL_USE", result.getData().getPrivacy());
        Assert.assertEquals("URGENTLY", result.getData().getUrgency());
        Assert.assertEquals(1, result.getData().getItems().size());
    }
}
