package ru.sberbank.cseodo.ms.apigateway.web.controllers;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import ru.sberbank.cseodo.ms.apigateway.dto.response.Response;
import ru.sberbank.cseodo.ms.apigateway.feign.ReferenceApiClient;
import ru.sberbank.cseodo.ms.apigateway.service.ReferenceService;

import java.util.HashMap;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = {ReferenceController.class,
        ReferenceService.class})
@MockBean(classes = {ReferenceApiClient.class})
public class ReferenceControllerTest {

    public static final String REFERENCE_RESPONSE = "Here must be some reference";

    @Autowired
    ReferenceController controller;

    @MockBean
    ReferenceApiClient apiClient;

    @Test
    public void getAllOk() {

        Mockito.when(apiClient.getAll()).thenReturn(REFERENCE_RESPONSE);

        Object response = controller.getAll(new HashMap<>());

        Assert.assertTrue(response instanceof String);
        Assert.assertEquals(REFERENCE_RESPONSE, response);
    }

    @Test
    public void getAllFail() {

        Mockito.when(apiClient.getAll()).thenThrow(new NullPointerException("всё упало"));

        Object response = controller.getAll(new HashMap<>());

        Assert.assertTrue(response instanceof Response);
        Assert.assertEquals("500", ((Response) response).getError().getCode());
        Assert.assertEquals("Ошибка при получении справочника: всё упало", ((Response) response).getError().getMessage());
    }
}
