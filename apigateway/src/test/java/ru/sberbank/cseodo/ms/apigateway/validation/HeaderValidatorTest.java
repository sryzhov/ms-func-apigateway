package ru.sberbank.cseodo.ms.apigateway.validation;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.validation.annotation.Validated;
import org.springframework.validation.beanvalidation.MethodValidationPostProcessor;

import javax.validation.ConstraintViolationException;
import java.util.Collections;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = {HeaderValidator.class, HeaderValidatorTest.Configuration.class})
public class HeaderValidatorTest {

    @Autowired
    TestValidHeader v;

    @Test
    public void validHeaderOk() {

        Assert.assertEquals(1,
                v.f(Collections.singletonMap("transactionid", "00000000-0000-0000-0000-000000000000")));
    }

    @Test
    public void validHeaderFailIsEmpty() {

        ConstraintViolationException ex = Assert.assertThrows(ConstraintViolationException.class, () ->
                v.f(Collections.emptyMap()));

        Assert.assertTrue(ex.getMessage().contains("f.map: Неверный заголовок сообщения"));
        Assert.assertTrue(ex.getMessage().contains("f.map: Не задан идентификатор транзакции(transactionId)"));
    }

    @Test
    public void validHeaderFailWrongFormat() {

        ConstraintViolationException ex = Assert.assertThrows(ConstraintViolationException.class, () ->
                v.f(Collections.singletonMap("transactionid", "0")));

        Assert.assertTrue(ex.getMessage().contains("f.map: Неверный заголовок сообщения"));
        Assert.assertTrue(ex.getMessage().contains("f.map: Идентификатор транзакции(transactionId) должен иметь формат UUID"));
    }

    @Validated
    interface TestValidHeader {
        int f(@ValidHeader Map<String, String> map);
    }

    static class TestValidHeaderImpl implements TestValidHeader {
        public int f(@ValidHeader Map<String, String> map) {
            return map.size();
        }
    }

    @TestConfiguration
    static class Configuration {
        @Bean
        public MethodValidationPostProcessor bean() {
            return new MethodValidationPostProcessor();
        }

        @Bean
        public TestValidHeader c() {
            return new TestValidHeaderImpl();
        }
    }
}
