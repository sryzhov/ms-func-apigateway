package ru.sberbank.cseodo.ms.apigateway.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.sberbank.cseodo.ms.apigateway.feign.CommentApiClient;

import java.util.Map;

@Service
@AllArgsConstructor
@Slf4j
public class CommentService {

    private final CommentApiClient apiClient;

    public Object getTreeComments(
            String entityType,
            String entityId,
            Integer depth,
            String parentId,
            Map<String, String> headerMap) {

        return apiClient.getTreeComments(entityType, entityId, depth, parentId, headerMap);
    }

    public Object addComment(
            String entityType,
            String entityId,
            String parentId,
            Object request,
            Map<String, String> headerMap) {
        return apiClient.addComment(entityType, entityId, parentId, request, headerMap);
    }

    public Object editComment(
            String entityType,
            String commentId,
            String entityId,
            String parentId,
            Object request,
            Map<String, String> headerMap) {

        return apiClient.editComment(entityType, entityId, parentId, commentId, request, headerMap);
    }

    public Object removeComment(
            String entityType,
            String commentId,
            String entityId,
            String parentId,
            Map<String, String> headerMap) {

        return apiClient.removeComment(entityType, entityId, parentId, commentId, headerMap);
    }

    public Object responseComment(
            String entityType,
            String commentId,
            String entityId,
            String parentId,
            Object requestDto,
            Map<String, String> headerMap) {

        return apiClient.responseComment(entityType, entityId, parentId, commentId, requestDto, headerMap);
    }

    public Object addAttachment(
            String commentId,
            Object dto,
            Map<String, String> headerMap) {

        return apiClient.addAttachment(commentId, dto, headerMap);
    }

    public String removeAttachment(
            String attachmentId,
            Map<String, String> headerMap) {

        return apiClient.removeAttachment(attachmentId, headerMap);
    }
}
