package ru.sberbank.cseodo.ms.apigateway.dto.attachment.back;

import lombok.Data;
import lombok.EqualsAndHashCode;
import ru.sberbank.cseodo.ms.apigateway.dto.attachment.ModifyPositionRequestDTO;

@EqualsAndHashCode(callSuper = true)
@Data
public class BackModifyPositiontRequestDTO extends ModifyPositionRequestDTO {
}
