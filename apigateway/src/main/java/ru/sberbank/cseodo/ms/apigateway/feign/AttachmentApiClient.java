package ru.sberbank.cseodo.ms.apigateway.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import ru.sberbank.cseodo.ms.apigateway.dto.attachment.back.*;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.Map;

@FeignClient(value = "${cseodo.service.attachment:attachment}/web/v0.1/attachment/",
        url = "${cseodo.service.attachment.url:}")
@Validated
public interface AttachmentApiClient {

    @GetMapping(value = "{parentType}/getAttachments/")
    Serializable getAttachments(@NotEmpty @PathVariable("parentType") String parentType,
                                @NotEmpty @RequestParam(value = "parentId") String parentId,
                                @RequestHeader Map<String, String> headerMap);

    @GetMapping(value = "{parentType}/getAttachmentInfo/")
    Serializable getAttachmentInfo(@NotEmpty @PathVariable("parentType") String parentType,
                                   @NotEmpty @RequestParam(value = "parentId") String parentId,
                                   @NotEmpty @RequestParam(value = "attachmentId") String attachmentId,
                                   @RequestHeader Map<String, String> headerMap);

    @PostMapping(value = "{parentType}/upload/")
    Serializable upload(@PathVariable("parentType") String parentType,
                        @RequestBody BackUploadAttachmentRequestDTO body,
                        @RequestHeader Map<String, String> headerMap);

    @PostMapping(value = "{parentType}/replace/")
    Serializable replace(@PathVariable("parentType") String parentType,
                         @RequestBody BackReplaceAttachmentRequestDTO body,
                         @RequestHeader Map<String, String> headerMap);

    @PostMapping(value = "{parentType}/remove/")
    Serializable remove(@PathVariable("parentType") String parentType,
                        @RequestBody BackAttachmentRequestDTO body,
                        @RequestHeader Map<String, String> headerMap);

    @PostMapping(value = "{parentType}/rename/")
    Serializable rename(@PathVariable("parentType") String parentType,
                        @RequestBody BackRenameAttachmentRequestDTO body,
                        @RequestHeader Map<String, String> headerMap);

    @PostMapping(value = "{parentType}/modifyPosition/")
    Serializable modifyPosition(@PathVariable("parentType") String parentType,
                                @RequestBody BackModifyPositiontRequestDTO body,
                                @RequestHeader Map<String, String> headerMap);

    @PostMapping(value = "{parentType}/removeAll/")
    Serializable removeAll(@PathVariable("parentType") String parentType,
                           @RequestBody Serializable body,
                           @RequestHeader Map<String, String> headerMap);

    @GetMapping(value = "{parentType}/getCapacity/")
    Serializable getCapacity(@PathVariable("parentType") String parentType,
                             @RequestParam(value = "parentId") String parentId,
                             @RequestHeader Map<String, String> headerMap);

    @PostMapping(value = "{parentType}/openEditor/")
    Serializable openEditor(@NotEmpty @PathVariable("parentType") String parentType,
                            @Valid @RequestBody BackEditorRequestDTO body,
                            @RequestHeader Map<String, String> headerMap);

    @GetMapping(value = "{parentType}/getVersions/")
    Serializable getVersions(@PathVariable("parentType") String parentType,
                             @RequestParam(value = "parentId") String parentId,
                             @RequestParam(value = "attachmentId") String attachmentId,
                             @RequestHeader Map<String, String> headerMap);

    @GetMapping(value = "{parentType}/getActualVersion/")
    Serializable getActualVersion(@PathVariable("parentType") String parentType,
                                  @RequestParam(value = "parentId") String parentId,
                                  @RequestParam(value = "attachmentId") String attachmentId,
                                  @RequestHeader Map<String, String> headerMap);

    @GetMapping(value = "{parentType}/getContent/")
    Serializable getContent(@PathVariable("parentType") String parentType,
                            @RequestParam(value = "parentId") String parentId,
                            @RequestParam(value = "attachmentId") String attachmentId,
                            @RequestParam(value = "majorVersion") String majorVersion,
                            @RequestParam(value = "minorVersion") String minorVersion,
                            @RequestHeader Map<String, String> headerMap);
}