package ru.sberbank.cseodo.ms.apigateway.dto.attachment;

import lombok.Data;

import java.io.Serializable;

@Data
public class ReplaceAttachmentRequestDTO {
    private String parentId;
    private String filename;
    private Serializable content;
    private Integer major;
    private Integer minor;
}
