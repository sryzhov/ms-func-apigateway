package ru.sberbank.cseodo.ms.apigateway.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.sberbank.cseodo.ms.apigateway.dto.attachment.back.*;
import ru.sberbank.cseodo.ms.apigateway.dto.attachment.front.FrontModifyPositionRequestDTO;
import ru.sberbank.cseodo.ms.apigateway.dto.attachment.front.FrontRenameAttachmentRequestDTO;
import ru.sberbank.cseodo.ms.apigateway.dto.attachment.front.FrontReplaceAttachmentRequestDTO;
import ru.sberbank.cseodo.ms.apigateway.dto.attachment.front.FrontUploadAttachmentRequestDTO;
import ru.sberbank.cseodo.ms.apigateway.feign.AttachmentApiClient;
import ru.sberbank.cseodo.ms.apigateway.mappers.AttachmentMapper;

import java.io.Serializable;
import java.util.Map;


@Service
@AllArgsConstructor
@Slf4j
public class AttachmentService {

    private final AttachmentApiClient attachmentApiClient;

    private final AttachmentMapper attachmentMapper;

    public Serializable getAttachments(String parentType, String parentId, Map<String, String> headerMap) {

        return attachmentApiClient.getAttachments(parentType, parentId, headerMap);
    }

    public Serializable getAttachmentInfo(String parentType, String parentId, String attachmentId, Map<String, String> headerMap) {

        return attachmentApiClient.getAttachmentInfo(parentType, parentId, attachmentId, headerMap);
    }

    public Serializable editor(String parentType, String parentId, String attachmentId, Integer majorVersion, Integer minorVersion,
                               Map<String, String> headerMap) {
        BackEditorRequestDTO body = new BackEditorRequestDTO();
        body.setParentId(parentId);
        body.setAttachmentId(attachmentId);
        body.setMajorVersion(majorVersion);
        body.setMinorVersion(minorVersion);
        return attachmentApiClient.openEditor(parentType, body, headerMap);
    }

    public Serializable upload(FrontUploadAttachmentRequestDTO body, Map<String, String> headerMap) {
        BackUploadAttachmentRequestDTO backDto = attachmentMapper.frontToBack(body);
        return attachmentApiClient.upload(body.getParentType(), backDto, headerMap);
    }

    public Serializable replace(String attachmentId, FrontReplaceAttachmentRequestDTO body, Map<String, String> headerMap) {

        BackReplaceAttachmentRequestDTO backDto = attachmentMapper.frontToBack(body);
        backDto.setAttachmentId(attachmentId);
        return attachmentApiClient.replace(body.getParentType(), backDto, headerMap);
    }

    public Serializable rename(String attachmentId, FrontRenameAttachmentRequestDTO body, Map<String, String> headerMap) {

        BackRenameAttachmentRequestDTO backDto = attachmentMapper.frontToBack(body);
        backDto.setAttachmentId(attachmentId);
        return attachmentApiClient.rename(body.getParentType(), backDto, headerMap);
    }

    public Serializable modifyPosition(FrontModifyPositionRequestDTO body, Map<String, String> headerMap) {

        BackModifyPositiontRequestDTO backDto = attachmentMapper.frontToBack(body);
        return attachmentApiClient.modifyPosition(body.getParentType(), backDto, headerMap);
    }

    public Serializable remove(String attachmentId, String parentType, String parentId, Map<String, String> headerMap) {
        BackAttachmentRequestDTO backDTO = new BackAttachmentRequestDTO();
        backDTO.setParentId(parentId);
        backDTO.setAttachmentId(attachmentId);
        return attachmentApiClient.remove(parentType, backDTO, headerMap);
    }

    public Serializable getCapacity(String parentType, String parentId, Map<String, String> headerMap) {
        return attachmentApiClient.getCapacity(parentType, parentId, headerMap);
    }
}
