package ru.sberbank.cseodo.ms.apigateway.dto.attachment.back;

import lombok.Data;
import lombok.EqualsAndHashCode;
import ru.sberbank.cseodo.ms.apigateway.dto.attachment.AttachmentRequestDTO;

@EqualsAndHashCode(callSuper = true)
@Data
public class BackAttachmentRequestDTO extends AttachmentRequestDTO {
    private String attachmentId;
}
