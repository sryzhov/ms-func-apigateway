package ru.sberbank.cseodo.ms.apigateway.web.exceptionhandlers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import ru.sberbank.cseodo.ms.apigateway.dto.response.Response;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static ru.sberbank.cseodo.ms.apigateway.utils.WebUtils.buildErrorResponse;

@RestControllerAdvice
@Order(Ordered.LOWEST_PRECEDENCE)
@Slf4j
public class DefaultExceptionController {

    @ExceptionHandler(RuntimeException.class)
    public Response<Void> handleRuntimeException(HttpServletRequest request, HttpServletResponse response, Throwable e) {

        log.error("Unexpected gateway error", e);

        return buildErrorResponse(500, String.format("Внутренняя ошибка: %s", e.getMessage()));
    }
}
