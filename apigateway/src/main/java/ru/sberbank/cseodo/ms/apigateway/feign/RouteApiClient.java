package ru.sberbank.cseodo.ms.apigateway.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import ru.sberbank.cseodo.ms.apigateway.dto.response.Response;
import ru.sberbank.cseodo.ms.apigateway.dto.route.RouteDataDto;
import ru.sberbank.cseodo.ms.apigateway.dto.route.RouteDto;
import ru.sberbank.cseodo.ms.apigateway.validation.ValidHeader;

import java.util.Map;
import java.util.UUID;

@FeignClient(value = "${cseodo.service.route.name:route}/web/v1/route",
        url = "${cseodo.service.route.url:}")
@Validated
public interface RouteApiClient {

    @PostMapping(value = "create")
    Response<RouteDto> create(@RequestParam UUID documentId,
                              @RequestBody RouteDataDto body,
                              @RequestHeader @ValidHeader Map<String, String> headerMap);

    @GetMapping(value = "{routeId}/{documentId}/get")
    Response<RouteDataDto> get(@PathVariable String routeId, @PathVariable UUID documentId,
                               @RequestParam Integer majorVersion, @RequestParam Integer minorVersion,
                               @RequestHeader @ValidHeader Map<String, String> headerMap);
}