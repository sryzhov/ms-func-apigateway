package ru.sberbank.cseodo.ms.apigateway.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(value = "${cseodo.service.reference.name:reference}/web/v0.1/reference",
        url = "${cseodo.service.reference.url:}")
public interface ReferenceApiClient {

    @GetMapping("/getAll/")
    public Object getAll();
}