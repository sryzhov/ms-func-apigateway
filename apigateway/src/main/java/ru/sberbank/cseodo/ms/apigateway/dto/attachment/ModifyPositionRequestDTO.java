package ru.sberbank.cseodo.ms.apigateway.dto.attachment;

import lombok.Data;

import java.util.List;

@Data
public class ModifyPositionRequestDTO {
    private String parentId;
    private List<String> identsOrder;
}
