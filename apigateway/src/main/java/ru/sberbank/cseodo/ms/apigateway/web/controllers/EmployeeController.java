package ru.sberbank.cseodo.ms.apigateway.web.controllers;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import ru.sberbank.cseodo.ms.apigateway.dto.response.GatewayResponse;
import ru.sberbank.cseodo.ms.apigateway.dto.response.Response;
import ru.sberbank.cseodo.ms.apigateway.service.EmployeeService;

import java.util.Map;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api/web/v0.1/employees")
@AllArgsConstructor
@Slf4j
public class EmployeeController {

    private final EmployeeService employeeService;

    @GetMapping
    public GatewayResponse search(@RequestParam("q") String q,
                                  @RequestParam(value = "type", required = false) String type,
                                  @RequestHeader Map<String, String> headerMap) {
        log.info("Got search request. header = {}, q = {},  type = {}", headerMap, q, type);
        GatewayResponse response = employeeService.search(q, type, headerMap);
        log.info("Sending searchForSenders response: {}", response);
        return response;
    }

    @GetMapping("{employeeId}")
    public Response<Object> get(@PathVariable(value = "employeeId") String employeeId,
                                             @RequestParam(value = "variant", required = false) String variant,
                                             @RequestHeader Map<String, String> headerMap) {
        log.info("Got searchForSenders request. header = {}, employeeId = {}, variant = {}", headerMap, employeeId, variant);
        Response<Object> response = employeeService.get(employeeId, variant, headerMap);
        log.info("Sending search response: {}", response);
        return response;
    }
}
