package ru.sberbank.cseodo.ms.apigateway.dto.document;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class BackDocumentItemDto {

    String itemGuid;
    String documentGuid;
    String itemCode;
    BigDecimal sortOrder;
    Boolean encrypted;
    String content;
}