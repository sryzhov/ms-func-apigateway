package ru.sberbank.cseodo.ms.apigateway.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.sberbank.cseodo.ms.apigateway.dto.numeration.NumerationDto;
import ru.sberbank.cseodo.ms.apigateway.dto.numeration.NumerationResult;
import ru.sberbank.cseodo.ms.apigateway.dto.response.Response;
import ru.sberbank.cseodo.ms.apigateway.feign.NumerationApiClient;

@Service
@AllArgsConstructor
@Slf4j
public class NumerationService {

    private final NumerationApiClient numerationApiClient;

    public String getNumber() {

        Response<NumerationResult> numerationResult = numerationApiClient.nextValue("ЦА", new NumerationDto());
        String documentNumber = numerationResult.isSuccess() ? numerationResult.getData().getSysNumber() : "";
        log.info("Acquired new document number {}", documentNumber);

        return documentNumber;
    }
}
