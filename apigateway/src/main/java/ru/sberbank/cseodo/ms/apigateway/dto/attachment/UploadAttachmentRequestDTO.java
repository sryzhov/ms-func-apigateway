package ru.sberbank.cseodo.ms.apigateway.dto.attachment;

import lombok.Data;

import java.io.Serializable;

@Data
public class UploadAttachmentRequestDTO {
    private String parentId;
    private String filename;
    private Serializable content;
}
