package ru.sberbank.cseodo.ms.apigateway.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import ru.sberbank.cseodo.ms.apigateway.dto.document.*;
import ru.sberbank.cseodo.ms.apigateway.dto.response.GatewayResponse;
import ru.sberbank.cseodo.ms.apigateway.dto.response.Response;
import ru.sberbank.cseodo.ms.apigateway.dto.route.RouteDataAuthorDto;
import ru.sberbank.cseodo.ms.apigateway.dto.route.RouteDto;
import ru.sberbank.cseodo.ms.apigateway.feign.DocumentApiClient;
import ru.sberbank.cseodo.ms.apigateway.feign.EmployeeApiClient;
import ru.sberbank.cseodo.ms.apigateway.mappers.DocumentItemFullMapper;
import ru.sberbank.cseodo.ms.apigateway.mappers.DocumentMapper;

import java.util.*;

import static ru.sberbank.cseodo.ms.apigateway.utils.DocumentUtils.buildDefaultDocument;
import static ru.sberbank.cseodo.ms.apigateway.utils.RouteUtils.buildDefaultRoute;
import static ru.sberbank.cseodo.ms.apigateway.utils.WebUtils.buildErrorResponse;
import static ru.sberbank.cseodo.ms.apigateway.utils.WebUtils.buildSuccessResponse;

@Service
@AllArgsConstructor
@Slf4j
public class DocumentService {

    private final DocumentApiClient documentApiClient;
    private final EmployeeApiClient employeeApiClient;

    NumerationService numerationService;
    RouteService routeService;

    private final DocumentMapper documentMapper;
    private final DocumentItemFullMapper documentItemFullMapper;

    public GatewayResponse get(UUID documentGuid,
                               Map<String, String> headerMap) {

        Map<String, String> postHeaders = new HashMap<>(headerMap);
        postHeaders.put("Content-Type", "application/json");

        Response<BackDocumentDto> documentResponse = documentApiClient.get(documentGuid, headerMap);
        if (!documentResponse.isSuccess()) {
            return buildErrorResponse(422,
                    String.format("Ошибка получения документа. %s", documentResponse.getError().getMessage()));
        }
        FrontDocumentRouteDto document =
                documentMapper.toRouteFront(
                        documentMapper.backToFront(
                                (documentResponse.getData())));
        document.setRoute(routeService.get(
                document.getRouteId(), documentGuid, document.getMajorVersion(), document.getMinorVersion(), postHeaders));

        return buildSuccessResponse(document);
    }

    public Response<List<FrontDocumentDto>> list(Long limit,
                                                 Map<String, String> headerMap) {

        return buildSuccessResponse(documentMapper.backToFront(documentApiClient.list(limit, headerMap)));
    }

    public Response<List<UUID>> idsList(Long limit,
                                        Map<String, String> headerMap) {

        return buildSuccessResponse(documentApiClient.idsList(limit, headerMap).getData());
    }

    public Response<FrontDocumentDto> create(String type, String kind, Map<String, String> headerMap) {

        BackDocumentDto backDto = buildDefaultDocument(type, kind, numerationService.getNumber());
        documentApiClient.create(backDto, headerMap);

        // Надо это убрать в мс Документ. Оно там сохраняется асинхронно и неспешно.
        for (int i = 1; i < 25; i++) {
            Response<BackDocumentDto> getResponse = documentApiClient.get(backDto.getDocumentGuid(), headerMap);
            if (getResponse.isSuccess() && !getResponse.getData().getItems().isEmpty()) {
                log.info("Got response on created document by {} iteration", i);
                return buildSuccessResponse(documentMapper.backToFront(getResponse.getData()));
            }
        }
        return buildErrorResponse(500,
                String.format("Ошибка при получении данных создаваемого документа %s", backDto.getDocumentGuid()));
    }

    public Response<String> edit(UUID guid, FrontDocumentDto frontDto, Map<String, String> headerMap) {

        if (frontDto.getGuid() == null) {
            frontDto.setGuid(guid.toString());
        }
        BackDocumentDto backDto = documentMapper.frontToBack(frontDto);
        backDto.setDocumentGuid(guid);
        documentApiClient.edit(backDto, headerMap);

        return buildSuccessResponse("OK");
    }

    public Response<String> delete(UUID guid,
                                   Map<String, String> headerMap) {

        BackDocumentDto backDocumentDto = new BackDocumentDto();
        backDocumentDto.setDocumentGuid(guid);

        documentApiClient.delete(backDocumentDto, headerMap);

        Response<String> successResponse = new Response<>();
        successResponse.setSuccess(true);
        successResponse.setData("OK");
        return successResponse;
    }

    public Response<String> addItem(UUID documentId,
                                    FrontDocumentItemFullDto itemDto,
                                    Map<String, String> headerMap) {
        BackDocumentItemDto backItemDto = documentItemFullMapper.frontToBack(itemDto);
        backItemDto.setDocumentGuid(documentId.toString());
        documentApiClient.addItem(backItemDto, headerMap);
        return buildSuccessResponse("OK");
    }

    public Response<String> editItem(UUID documentId,
                                     UUID itemId,
                                     FrontDocumentItemFullDto itemDto,
                                     Map<String, String> headerMap) {
        BackDocumentItemDto backItemDto = documentItemFullMapper.frontToBack(itemDto);
        backItemDto.setDocumentGuid(documentId.toString());
        backItemDto.setItemGuid(itemId.toString());
        documentApiClient.editItem(backItemDto, headerMap);
        return buildSuccessResponse("OK");
    }

    public Response<String> removeItem(UUID documentId,
                                       UUID itemId,
                                       Map<String, String> headerMap) {
        BackDocumentItemDto backItemDto = new BackDocumentItemDto();
        backItemDto.setDocumentGuid(documentId.toString());
        backItemDto.setItemGuid(itemId.toString());
        documentApiClient.removeItem(backItemDto, headerMap);
        return buildSuccessResponse("OK");
    }

    public GatewayResponse newRoute(@PathVariable(value = "guid") UUID documentId,
                                    @RequestHeader Map<String, String> headerMap) {

        Long userId = Optional.ofNullable(headerMap.get("userid")).map(Long::parseLong).orElse(null);
        if (userId == null) {
            return buildErrorResponse(401, "Не указан идентификатор пользователя");
        }
        Response<Object> employeeResponse = employeeApiClient.search(userId, "full", headerMap);
        if (!employeeResponse.isSuccess()) {
            return buildErrorResponse(500, String.format("Ошибка получения данных сотрудника: %s",
                    employeeResponse.getError().getMessage()));
        }
        Response<RouteDataAuthorDto> signersResponse = employeeApiClient.getSigners(userId, headerMap);
        if (!signersResponse.isSuccess()) {
            return buildErrorResponse(500, String.format("Ошибка получения маршрута согласования: %s",
                    signersResponse.getError().getMessage()));
        }
        signersResponse.getData().setRoute(buildDefaultRoute(documentId));

        Response<RouteDto> routeResponse = routeService.create(documentId, signersResponse.getData(), headerMap);
        if (!routeResponse.isSuccess()) {
            return buildErrorResponse(500, String.format("Ошибка сохранения маршрута согласования: %s",
                    routeResponse.getError().getMessage()));
        }
        Response<BackDocumentDto> documentResponse = documentApiClient.get(documentId, headerMap);
        if (!documentResponse.isSuccess()) {
            return buildErrorResponse(500, String.format("Ошибка обновления документа: %s",
                    routeResponse.getError().getMessage()));
        }
        documentResponse.getData().setRouteId(routeResponse.getData().getGuid());
        documentResponse.getData().setMajorVersion(routeResponse.getData().getDocument().getVersion().getMajor());
        documentResponse.getData().setMinorVersion(routeResponse.getData().getDocument().getVersion().getMinor());
        documentApiClient.edit(documentMapper.backToBackWithDictToString(documentResponse.getData()), headerMap);

        signersResponse.getData().setAuthor(employeeResponse.getData());

        return buildSuccessResponse(signersResponse.getData());
    }
}
