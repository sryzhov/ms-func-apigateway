package ru.sberbank.cseodo.ms.apigateway.dto.document;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class FrontDocumentItemFullDto extends FrontDocumentItemDto {

    String documentGuid;
}
