package ru.sberbank.cseodo.ms.apigateway.dto.attachment.back;

import lombok.Data;
import lombok.EqualsAndHashCode;
import ru.sberbank.cseodo.ms.apigateway.dto.attachment.EditorRequestDTO;

@EqualsAndHashCode(callSuper = true)
@Data
public class BackEditorRequestDTO extends EditorRequestDTO {
    private String attachmentId;
}
