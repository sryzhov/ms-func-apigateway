package ru.sberbank.cseodo.ms.apigateway.dto.attachment.back;

import lombok.Data;
import lombok.EqualsAndHashCode;
import ru.sberbank.cseodo.ms.apigateway.dto.attachment.UploadAttachmentRequestDTO;

@EqualsAndHashCode(callSuper = true)
@Data
public class BackUploadAttachmentRequestDTO extends UploadAttachmentRequestDTO {

}
