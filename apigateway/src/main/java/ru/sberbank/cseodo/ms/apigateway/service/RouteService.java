package ru.sberbank.cseodo.ms.apigateway.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.sberbank.cseodo.ms.apigateway.dto.response.Response;
import ru.sberbank.cseodo.ms.apigateway.dto.route.RouteDataDto;
import ru.sberbank.cseodo.ms.apigateway.dto.route.RouteDto;
import ru.sberbank.cseodo.ms.apigateway.feign.RouteApiClient;

import java.util.Map;
import java.util.Optional;
import java.util.UUID;

@Service
@AllArgsConstructor
@Slf4j
public class RouteService {

    private static final Integer DEFAULT_MAJOR_VERSION = 0;
    private static final Integer DEFAULT_MINOR_VERSION = 0;

    private final RouteApiClient routeApiClient;

    public RouteDataDto get(String routeId, UUID documentId, Integer majorVersion, Integer minorVersion,
                            Map<String, String> headerMap) {
        if (routeId != null) {
            return routeApiClient.get(routeId, documentId,
                    Optional.ofNullable(majorVersion).orElse(DEFAULT_MAJOR_VERSION),
                    Optional.ofNullable(minorVersion).orElse(DEFAULT_MINOR_VERSION),
                    headerMap).getData();
        } else {
            log.warn("Документ id = {}. routeId = null. Запрос маршрута не выполняется.", documentId);
            return null;
        }
    }

    public Response<RouteDto> create(UUID documentId, RouteDataDto body, Map<String, String> headerMap) {
        return routeApiClient.create(documentId, body, headerMap);
    }
}
