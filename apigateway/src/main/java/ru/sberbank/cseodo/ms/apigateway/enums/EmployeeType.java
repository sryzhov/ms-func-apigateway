package ru.sberbank.cseodo.ms.apigateway.enums;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.experimental.FieldDefaults;
import org.apache.commons.lang.StringUtils;

import java.util.stream.Collectors;
import java.util.stream.Stream;

@Getter
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public enum EmployeeType {

    RECIPIENT("recipient"),
    SENDER("sender"),
    UNDEFINED("");

    private final String value;

    public static EmployeeType find(String value) {
        for (EmployeeType type : EmployeeType.values()) {
            if (type.value.equals(value)) {
                return type;
            }
        }
        return UNDEFINED;
    }

    public static String stringOfValidValues() {
        return Stream.of(EmployeeType.values())
                .map(EmployeeType::getValue)
                .filter(v -> !StringUtils.isEmpty(v))
                .collect(Collectors.joining(", "));
    }
}
