package ru.sberbank.cseodo.ms.apigateway.dto.document;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Map;

@Data
public class FrontDocumentItemDto {

    String guid;
    String code;
    BigDecimal sortOrder;
    Boolean encrypted;
    Map<String, Object> content;
}
