package ru.sberbank.cseodo.ms.apigateway.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.sberbank.cseodo.ms.apigateway.dto.response.GatewayResponse;
import ru.sberbank.cseodo.ms.apigateway.dto.response.Response;
import ru.sberbank.cseodo.ms.apigateway.enums.EmployeeType;
import ru.sberbank.cseodo.ms.apigateway.feign.EmployeeApiClient;

import java.util.Map;

import static ru.sberbank.cseodo.ms.apigateway.utils.WebUtils.buildErrorResponse;
import static ru.sberbank.cseodo.ms.apigateway.utils.WebUtils.buildSuccessResponse;

@Service
@AllArgsConstructor
@Slf4j
public class EmployeeService {

    private final EmployeeApiClient employeeApiClient;

    public GatewayResponse search(String query, String employeeType, Map<String, String> headerMap) {

        EmployeeType type = EmployeeType.find(employeeType);
        Response<Object> result;
        switch (type) {
            case RECIPIENT:
                result = employeeApiClient.searchForAddressees(query, null, null, headerMap);
                break;
            case SENDER:
                result = employeeApiClient.searchForSenders(query, null, null, headerMap);
                break;
            default:
                String errorMessage = String.format("%s - неверный тип поиска сотрудника. Допустимые значения: %s",
                        employeeType, EmployeeType.stringOfValidValues());
                return buildErrorResponse(400, errorMessage);
        }
        return buildSuccessResponse(result.getData());
    }

    public Response<Object> get(String id, String variant, Map<String, String> headerMap) {
        return buildSuccessResponse(employeeApiClient.search(Long.parseLong(id), variant, headerMap).getData());
    }
}
