package ru.sberbank.cseodo.ms.apigateway.dto.document;

import lombok.Data;
import lombok.EqualsAndHashCode;
import ru.sberbank.cseodo.ms.apigateway.dto.route.RouteDataDto;

@EqualsAndHashCode(callSuper = true)
@Data
public class FrontDocumentRouteDto extends FrontDocumentDto{

    RouteDataDto route;
}
