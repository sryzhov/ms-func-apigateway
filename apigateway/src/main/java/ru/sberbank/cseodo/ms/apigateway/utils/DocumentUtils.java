package ru.sberbank.cseodo.ms.apigateway.utils;

import ru.sberbank.cseodo.ms.apigateway.dto.document.BackDocumentDto;
import ru.sberbank.cseodo.ms.apigateway.dto.document.BackDocumentItemDto;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.UUID;

public class DocumentUtils {

    private static final String DEFAULT_PRIVACY = "FOR_INTERNAL_USE";
    private static final String DEFAULT_URGENCY = "URGENTLY";

    private static final DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");

    private DocumentUtils() {
    }

    public static BackDocumentDto buildDefaultDocument(String type, String kind, String documentNumber) {

        BackDocumentDto backDto = new BackDocumentDto();

        backDto.setDocumentType(type);
        backDto.setDocumentKind(kind);

        backDto.setDocumentGuid(UUID.randomUUID());
        backDto.setDocumentNumber(documentNumber);
        backDto.setCreationDate(LocalDate.now().format(dateFormatter));

        backDto.setItems(Collections.singletonList(
                buildItem("SECTION", -1)));

        backDto.setPrivacy(DEFAULT_PRIVACY);
        backDto.setUrgency(DEFAULT_URGENCY);

        return backDto;
    }

    private static BackDocumentItemDto buildItem(String itemCode, int itemSortOrder) {

        BackDocumentItemDto item = new BackDocumentItemDto();
        item.setItemGuid(UUID.randomUUID().toString());
        item.setItemCode(itemCode);
        item.setSortOrder(BigDecimal.valueOf(itemSortOrder));
        item.setEncrypted(false);
        item.setContent("");

        return item;
    }
}
