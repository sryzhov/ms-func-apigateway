package ru.sberbank.cseodo.ms.apigateway.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@FeignClient(value = "${cseodo.service.comment:comment}/web/v1/comment/",
        url = "${cseodo.service.comment.url:}")
public interface CommentApiClient {

    @GetMapping("{entityType}/getTreeComments/")
    public Object getTreeComments(
            @PathVariable String entityType,
            @RequestParam String entityId,
            @RequestParam(required = false) Integer depth,
            @RequestParam(required = false) String parentId,
            @RequestHeader Map<String, String> headerMap);

    @PostMapping("{entityType}/addComment/")
    public Object addComment(
            @PathVariable String entityType,
            @RequestParam String entityId,
            @RequestParam(required = false) String parentId,
            @RequestBody Object request,
            @RequestHeader Map<String, String> headerMap);

    @PostMapping("{entityType}/editComment/")
    public Object editComment(
            @PathVariable String entityType,
            @RequestParam String entityId,
            @RequestParam(required = false) String parentId,
            @RequestParam String commentId,
            @RequestBody Object request,
            @RequestHeader Map<String, String> headerMap);

    @PostMapping("{entityType}/remove/")
    public Object removeComment(
            @PathVariable String entityType,
            @RequestParam String entityId,
            @RequestParam(required = false) String parentId,
            @RequestParam String commentId,
            @RequestHeader Map<String, String> headerMap);

    @PostMapping("{entityType}/response/")
    public Object responseComment(
            @PathVariable String entityType,
            @RequestParam String entityId,
            @RequestParam(required = false) String parentId,
            @RequestParam String commentId,
            @RequestBody Object requestDto,
            @RequestHeader Map<String, String> headerMap);

    @PostMapping("addAttachment")
    public Object addAttachment(
            @RequestParam String commentId,
            @RequestBody Object dto,
            @RequestHeader Map<String, String> headerMap);

    @PostMapping("removeAttachment")
    public String removeAttachment(
            @RequestParam String attachmentId,
            @RequestHeader Map<String, String> headerMap);
}