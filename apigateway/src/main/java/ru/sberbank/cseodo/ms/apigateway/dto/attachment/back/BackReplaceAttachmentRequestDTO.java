package ru.sberbank.cseodo.ms.apigateway.dto.attachment.back;

import lombok.Data;
import lombok.EqualsAndHashCode;
import ru.sberbank.cseodo.ms.apigateway.dto.attachment.ReplaceAttachmentRequestDTO;

@EqualsAndHashCode(callSuper = true)
@Data
public class BackReplaceAttachmentRequestDTO extends ReplaceAttachmentRequestDTO {
    private String attachmentId;
}
