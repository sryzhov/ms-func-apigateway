package ru.sberbank.cseodo.ms.apigateway.dto.attachment.back;

import lombok.Data;
import lombok.EqualsAndHashCode;
import ru.sberbank.cseodo.ms.apigateway.dto.attachment.RenameAttachmentRequestDTO;

@EqualsAndHashCode(callSuper = true)
@Data
public class BackRenameAttachmentRequestDTO extends RenameAttachmentRequestDTO {
    private String attachmentId;
}
