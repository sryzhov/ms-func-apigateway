package ru.sberbank.cseodo.ms.apigateway.dto.numeration;

import lombok.Data;

@Data
public class NumerationResult {

    String sysNumber;
}
