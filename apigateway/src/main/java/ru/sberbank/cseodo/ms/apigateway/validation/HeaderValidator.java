package ru.sberbank.cseodo.ms.apigateway.validation;

import org.apache.commons.lang.StringUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.constraintvalidation.SupportedValidationTarget;
import javax.validation.constraintvalidation.ValidationTarget;
import java.util.Map;
import java.util.UUID;

@SupportedValidationTarget(ValidationTarget.ANNOTATED_ELEMENT)
public class HeaderValidator implements
        ConstraintValidator<ValidHeader, Map<String, String>> {
    @Override
    public void initialize(ValidHeader constraintAnnotation) {

    }

    @Override
    public boolean isValid(Map<String, String> header, ConstraintValidatorContext context) {

        String transactionId = header.get("transactionid");
        String errorMessage = null;
        if (StringUtils.isEmpty(transactionId)) {
            errorMessage = "Не задан идентификатор транзакции(transactionId)";
        } else {
            try {
                UUID.fromString(transactionId);
            } catch (IllegalArgumentException exception) {
                errorMessage = "Идентификатор транзакции(transactionId) должен иметь формат UUID";
            }
        }
        if(errorMessage == null){
            return true;
        }
        context.buildConstraintViolationWithTemplate(errorMessage).addConstraintViolation();
        return false;
    }
}
