package ru.sberbank.cseodo.ms.apigateway.web.controllers;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import ru.sberbank.cseodo.ms.apigateway.service.CommentService;

import java.util.Map;

import static ru.sberbank.cseodo.ms.apigateway.utils.WebUtils.fixPostHeaders;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api/web/v0.1/comments")
@AllArgsConstructor
@Slf4j
public class CommentController {

    CommentService commentService;

    @GetMapping("{entityType}/tree")
    public Object getTreeComments(
            @PathVariable String entityType,
            @RequestParam String entityId,
            @RequestParam(required = false) Integer depth,
            @RequestParam(required = false) String parentId,
            @RequestHeader Map<String, String> headerMap) {

        log.info("Got getTreeComments request. entityType = {}, entityId = {},  depth = {}, parentId = {}, headerMap = {}",
                entityType, entityId, depth, parentId, headerMap);
        Object response = commentService.getTreeComments(entityType, entityId, depth, parentId, headerMap);
        log.info("Sending getTreeComments response: {}", response);
        return response;
    }

    @PostMapping("{entityType}")
    public Object addComment(
            @PathVariable String entityType,
            @RequestParam String entityId,
            @RequestParam(required = false) String parentId,
            @RequestBody Object request,
            @RequestHeader Map<String, String> headerMap) {

        log.info("Got addComment request. entityType = {}, entityId = {}, parentId = {}, request = {}, headerMap = {}",
                entityType, entityId, parentId, request, headerMap);
        Object response = commentService.addComment(entityType, entityId, parentId, request, fixPostHeaders(headerMap));
        log.info("Sending addComment response: {}", response);
        return response;
    }

    @PostMapping("{entityType}/{commentId}")
    public Object editComment(
            @PathVariable String entityType,
            @PathVariable String commentId,
            @RequestParam String entityId,
            @RequestParam(required = false) String parentId,
            @RequestBody Object request,
            @RequestHeader Map<String, String> headerMap) {

        log.info("Got editComment request. entityType = {}, commentId ={}, entityId = {}, parentId = {}, request = {}, headerMap = {}",
                entityType, commentId, entityId, parentId, request, headerMap);
        Object response = commentService.editComment(entityType, commentId, entityId, parentId, request, fixPostHeaders(headerMap));
        log.info("Sending editComment response: {}", response);
        return response;
    }

    @DeleteMapping("{entityType}/{commentId}")
    public Object removeComment(
            @PathVariable String entityType,
            @PathVariable String commentId,
            @RequestParam String entityId,
            @RequestParam(required = false) String parentId,
            @RequestHeader Map<String, String> headerMap) {

        log.info("Got removeComment request. entityType = {}, commentId ={}, entityId = {}, parentId = {}, headerMap = {}",
                entityType, commentId, entityId, parentId, headerMap);
        Object response = commentService.removeComment(entityType, commentId, entityId, parentId, headerMap);
        log.info("Sending removeComment response: {}", response);
        return response;
    }

    @PostMapping("{entityType}/{commentId}/response")
    public Object responseComment(
            @PathVariable String entityType,
            @PathVariable String commentId,
            @RequestParam String entityId,
            @RequestParam(required = false) String parentId,
            @RequestBody Object requestDto,
            @RequestHeader Map<String, String> headerMap) {

        log.info("Got responseComment request. entityType = {}, commentId ={}, entityId = {}, parentId = {}, requestDto = {}, headerMap = {}",
                entityType, commentId, entityId, parentId, requestDto, headerMap);
        Object response = commentService.responseComment(entityType, commentId, entityId, parentId, requestDto, fixPostHeaders(headerMap));
        log.info("Sending responseComment response: {}", response);
        return response;
    }

    @PostMapping("{entityType}/{commentId}/attachment")
    public Object addAttachment(
            @PathVariable String entityType,
            @PathVariable String commentId,
            @RequestBody Object dto,
            @RequestHeader Map<String, String> headerMap) {

        log.info("Got addAttachment request. entityType = {}, commentId ={}, dto = {}, headerMap = {}",
                entityType, commentId, dto, headerMap);
        Object response = commentService.addAttachment(commentId, dto, fixPostHeaders(headerMap));
        log.info("Sending addAttachment response: {}", response);
        return response;
    }

    @DeleteMapping("{entityType}/{commentId}/attachment/{attachmentId}")
    public String removeAttachment(
            @PathVariable String entityType,
            @PathVariable String commentId,
            @PathVariable String attachmentId,
            @RequestHeader Map<String, String> headerMap) {

        log.info("Got removeAttachment request. entityType = {}, commentId ={}, attachmentId = {}, headerMap = {}",
                entityType, commentId, attachmentId, headerMap);
        String response = commentService.removeAttachment(attachmentId, headerMap);
        log.info("Sending removeAttachment response: {}", response);
        return response;
    }
}
