package ru.sberbank.cseodo.ms.apigateway.dto.route;

import lombok.Data;

import java.util.UUID;

@Data
public class RouteDocumentDto {

    UUID guid;

    RouteDocumentVersionDto version;
}
