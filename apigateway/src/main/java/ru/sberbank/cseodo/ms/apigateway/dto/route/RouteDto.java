package ru.sberbank.cseodo.ms.apigateway.dto.route;

import lombok.Data;

import java.util.UUID;

@Data
public class RouteDto {

    UUID guid;

    RouteStatusDto status;

    RouteDocumentDto document;
}
