package ru.sberbank.cseodo.ms.apigateway.dto.attachment.front;

import lombok.Data;
import lombok.EqualsAndHashCode;
import ru.sberbank.cseodo.ms.apigateway.dto.attachment.ModifyPositionRequestDTO;

@EqualsAndHashCode(callSuper = true)
@Data
public class FrontModifyPositionRequestDTO extends ModifyPositionRequestDTO {
    String parentType;
}
