package ru.sberbank.cseodo.ms.apigateway.mappers;

import org.mapstruct.Mapper;
import ru.sberbank.cseodo.ms.apigateway.dto.attachment.back.BackModifyPositiontRequestDTO;
import ru.sberbank.cseodo.ms.apigateway.dto.attachment.back.BackRenameAttachmentRequestDTO;
import ru.sberbank.cseodo.ms.apigateway.dto.attachment.back.BackReplaceAttachmentRequestDTO;
import ru.sberbank.cseodo.ms.apigateway.dto.attachment.back.BackUploadAttachmentRequestDTO;
import ru.sberbank.cseodo.ms.apigateway.dto.attachment.front.FrontModifyPositionRequestDTO;
import ru.sberbank.cseodo.ms.apigateway.dto.attachment.front.FrontRenameAttachmentRequestDTO;
import ru.sberbank.cseodo.ms.apigateway.dto.attachment.front.FrontReplaceAttachmentRequestDTO;
import ru.sberbank.cseodo.ms.apigateway.dto.attachment.front.FrontUploadAttachmentRequestDTO;

@Mapper(componentModel = "spring", uses = DocumentItemMapper.class)
public interface AttachmentMapper {

    BackReplaceAttachmentRequestDTO frontToBack(FrontReplaceAttachmentRequestDTO source);

    BackUploadAttachmentRequestDTO frontToBack(FrontUploadAttachmentRequestDTO source);

    BackRenameAttachmentRequestDTO frontToBack(FrontRenameAttachmentRequestDTO source);

    BackModifyPositiontRequestDTO frontToBack(FrontModifyPositionRequestDTO source);
}