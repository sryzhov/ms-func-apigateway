package ru.sberbank.cseodo.ms.apigateway.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.sberbank.cseodo.ms.apigateway.dto.document.BackDocumentItemDto;
import ru.sberbank.cseodo.ms.apigateway.dto.document.FrontDocumentItemFullDto;

import java.util.UUID;

@Mapper(componentModel = "spring", imports = UUID.class)
public abstract class DocumentItemFullMapper extends JsonMapper{

    @Mapping(source = "guid", target = "itemGuid", defaultExpression ="java(UUID.randomUUID().toString())")
    @Mapping(source = "code", target = "itemCode")
    public abstract BackDocumentItemDto frontToBack(FrontDocumentItemFullDto source);
}