package ru.sberbank.cseodo.ms.apigateway.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.sberbank.cseodo.ms.apigateway.feign.ReferenceApiClient;

import static ru.sberbank.cseodo.ms.apigateway.utils.WebUtils.buildErrorResponse;

@Service
@AllArgsConstructor
@Slf4j
public class ReferenceService {

    private static final String ERROR_MSG = "Ошибка при получении справочника";
    private final ReferenceApiClient referenceApiClient;

    public Object getAll() {
        try {
            return referenceApiClient.getAll();
        } catch (Exception e) {
            log.error(ERROR_MSG, e);
            return buildErrorResponse(500, String.format("%s: %s", ERROR_MSG, e.getMessage()));
        }
    }
}
