package ru.sberbank.cseodo.ms.apigateway.dto.route;

import lombok.Data;

@Data
public class RouteDataDto {

    RouteDto route;

    Object agreements;

    Object senders;
}
