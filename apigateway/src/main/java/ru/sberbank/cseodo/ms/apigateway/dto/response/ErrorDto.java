package ru.sberbank.cseodo.ms.apigateway.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

@Data
@AllArgsConstructor
public class ErrorDto implements Serializable {

    String code;

    String message;
}
