package ru.sberbank.cseodo.ms.apigateway.web.controllers;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import ru.sberbank.cseodo.ms.apigateway.dto.document.FrontDocumentDto;
import ru.sberbank.cseodo.ms.apigateway.dto.response.GatewayResponse;
import ru.sberbank.cseodo.ms.apigateway.dto.response.Response;
import ru.sberbank.cseodo.ms.apigateway.service.DocumentService;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import static ru.sberbank.cseodo.ms.apigateway.utils.WebUtils.fixPostHeaders;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/web/v0.1/documents")
@AllArgsConstructor
@Slf4j
public class DocumentController {

    private final DocumentService documentService;

    @GetMapping("{guid}")
    public GatewayResponse get(@PathVariable(value = "guid") UUID guid,
                               @RequestHeader Map<String, String> headerMap) {
        log.info("Got get request. header = {}, documentGuid = {} ", headerMap, guid);
        GatewayResponse response = documentService.get(guid, headerMap);
        log.info("Sending get response: {}", response);
        return response;
    }

    @GetMapping
    public Response<List<FrontDocumentDto>> list(@RequestParam(value = "limit") Long limit,
                                                 @RequestHeader Map<String, String> headerMap) {
        log.info("Got list request. header = {}, limit = {} ", headerMap, limit);
        Response<List<FrontDocumentDto>> response = documentService.list(limit, headerMap);
        log.info("Sending list response: {}", response);
        return response;
    }

    @GetMapping("ids")
    public Response<List<UUID>> idsList(@RequestParam(value = "limit") Long limit,
                                        Map<String, String> headerMap) {
        log.info("Got idsList request. header = {}, limit = {} ", headerMap, limit);
        Response<List<UUID>> response = documentService.idsList(limit, headerMap);
        log.info("Sending idsList response: {}", response);
        return response;
    }

    @PostMapping
    public Response<FrontDocumentDto> create(@RequestBody FrontDocumentDto document,
                                             @RequestHeader Map<String, String> headerMap) {
        log.info("Got create request {}", document);
        Response<FrontDocumentDto> response =
                documentService.create((String) document.getType(), (String) document.getKind(), fixPostHeaders(headerMap));
        log.info("Sending create response: {}", response);
        return response;
    }

    @PostMapping("{guid}")
    public Response<String> update(@PathVariable(value = "guid") UUID guid,
                                   @RequestBody FrontDocumentDto document,
                                   @RequestHeader Map<String, String> headerMap) {
        log.info("Got update request guid = {}, document = {}", guid, document);
        Response<String> response =
                documentService.edit(guid, document, fixPostHeaders(headerMap));
        log.info("Sending update response: {}", response);
        return response;
    }

    @DeleteMapping("{guid}")
    public Response<String> delete(@PathVariable(value = "guid") UUID guid,
                                   @RequestHeader Map<String, String> headerMap) {
        log.info("Got delete request. guid = {}", guid);
        Response<String> response = documentService.delete(guid, fixPostHeaders(headerMap));
        log.info("Sending delete response: {}", response);
        return response;
    }

    @GetMapping("{guid}/route")
    public GatewayResponse newRoute(@PathVariable(value = "guid") UUID guid,
                                    @RequestHeader Map<String, String> headerMap) {
        log.info("Got newRoute request. guid = {}", guid);
        GatewayResponse response = documentService.newRoute(guid, headerMap);
        log.info("Sending newRoute response: {}", response);
        return response;
    }
}