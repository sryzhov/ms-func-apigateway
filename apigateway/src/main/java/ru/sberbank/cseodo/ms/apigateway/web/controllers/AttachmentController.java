package ru.sberbank.cseodo.ms.apigateway.web.controllers;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import ru.sberbank.cseodo.ms.apigateway.dto.attachment.front.FrontModifyPositionRequestDTO;
import ru.sberbank.cseodo.ms.apigateway.dto.attachment.front.FrontRenameAttachmentRequestDTO;
import ru.sberbank.cseodo.ms.apigateway.dto.attachment.front.FrontReplaceAttachmentRequestDTO;
import ru.sberbank.cseodo.ms.apigateway.dto.attachment.front.FrontUploadAttachmentRequestDTO;
import ru.sberbank.cseodo.ms.apigateway.service.AttachmentService;

import java.io.Serializable;
import java.util.Map;

import static ru.sberbank.cseodo.ms.apigateway.utils.WebUtils.fixPostHeaders;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api/web/v0.1/attachments")
@AllArgsConstructor
@Slf4j
public class AttachmentController {

    private final AttachmentService attachmentService;

    @GetMapping
    public Serializable list(@RequestParam(value = "limit") Long limit,
                             //required = false - чтобы дальше вернуть внятное сообщение об ошибке
                             @RequestParam(value = "parentType", required = false) String parentType,
                             @RequestParam(value = "parentId", required = false) String parentId,
                             @RequestHeader Map<String, String> headerMap) {
        log.info("Got getAttachments request. header = {}, parentType = {},  parentId = {}", headerMap, parentType, parentId);
        Serializable response = attachmentService.getAttachments(parentType, parentId, headerMap);
        log.info("Sending getAttachments response: {}", response);
        return response;
    }

    @GetMapping("{attachmentId}")
    public Serializable get(@PathVariable(value = "attachmentId", required = false) String attachmentId,
                            @RequestParam(value = "parentType", required = false) String parentType,
                            @RequestParam(value = "parentId", required = false) String parentId,
                            @RequestHeader Map<String, String> headerMap) {
        log.info("Got getAttachmentInfo request. header = {}, parentType = {},  parentId = {}, attachmentId = {} ",
                headerMap, parentType, parentId, attachmentId);
        Serializable response = attachmentService.getAttachmentInfo(parentType, parentId, attachmentId, headerMap);
        log.info("Sending getAttachmentInfo response: {}", response);
        return response;
    }

    @GetMapping("{attachmentId}/editor")
    public Serializable editor(@PathVariable(value = "attachmentId") String attachmentId,
                               @RequestParam(value = "parentType", required = false) String parentType,
                               @RequestParam(value = "parentId", required = false) String parentId,
                               @RequestParam(value = "majorVersion", required = false) Integer majorVersion,
                               @RequestParam(value = "minorVersion", required = false) Integer minorVersion,
                               @RequestHeader Map<String, String> headerMap) {
        log.info("Got editor request. header = {}, parentType = {},  parentId = {}, attachmentId = {}б majorVersion = {}, minorVersion = {}",
                headerMap, parentType, parentId, attachmentId, majorVersion, minorVersion);
        Serializable response = attachmentService.editor(parentType, parentId, attachmentId, majorVersion, minorVersion, headerMap);
        log.info("Sending editor response: {}", response);
        return response;
    }

    @PostMapping
    public Serializable upload(@RequestBody FrontUploadAttachmentRequestDTO body,
                               @RequestHeader Map<String, String> headerMap) {
        log.info("Got upload request. header = {}, body = {}", headerMap, body);
        Serializable response = attachmentService.upload(body, fixPostHeaders(headerMap));
        log.info("Sending upload response: {}", response);
        return response;
    }

    @PostMapping("{attachmentId}")
    public Serializable replace(@PathVariable(value = "attachmentId") String attachmentId,
                                @RequestBody FrontReplaceAttachmentRequestDTO body,
                                Map<String, String> headerMap) {
        log.info("Got replace request. header = {}, attachmentId = {},  body = {}", headerMap, attachmentId, body);
        Serializable response = attachmentService.replace(attachmentId, body, fixPostHeaders(headerMap));
        log.info("Sending replace response: {}", response);
        return response;
    }

    @PostMapping("{attachmentId}/rename")
    public Serializable rename(@PathVariable(value = "attachmentId") String attachmentId,
                               @RequestBody FrontRenameAttachmentRequestDTO body,
                               Map<String, String> headerMap) {
        log.info("Got rename request. header = {}, attachmentId = {},  body = {}", headerMap, attachmentId, body);
        Serializable response = attachmentService.rename(attachmentId, body, fixPostHeaders(headerMap));
        log.info("Sending rename response: {}", response);
        return response;
    }

    @PostMapping("/modifyPosition")
    public Serializable modifyPosition(@RequestBody FrontModifyPositionRequestDTO body,
                                       Map<String, String> headerMap) {
        log.info("Got modifyPosition request. header = {},  body = {}", headerMap, body);
        Serializable response = attachmentService.modifyPosition(body, fixPostHeaders(headerMap));
        log.info("Sending modifyPosition response: {}", response);
        return response;
    }

    @DeleteMapping("{attachmentId}")
    public Serializable remove(@PathVariable(value = "attachmentId") String attachmentId,
                               @RequestParam(value = "parentType") String parentType,
                               @RequestParam(value = "parentId") String parentId,
                               Map<String, String> headerMap) {
        log.info("Got remove request. header = {}, attachmentId = {}, parentType = {},  parentId = {}", headerMap, attachmentId, parentType, parentId);
        Serializable response = attachmentService.remove(attachmentId, parentType, parentId, fixPostHeaders(headerMap));
        log.info("Sending remove response: {}", response);
        return response;
    }

    @GetMapping("capacity")
    public Serializable getCapacity(@RequestParam(value = "parentType") String parentType,
                                    @RequestParam(value = "parentId") String parentId,
                                    Map<String, String> headerMap) {
        log.info("Got getCapacity request. header = {}, parentType = {},  parentId = {}", headerMap, parentType, parentId);
        Serializable response = attachmentService.getCapacity(parentType, parentId, fixPostHeaders(headerMap));
        log.info("Sending getCapacity response: {}", response);
        return response;
    }
}
