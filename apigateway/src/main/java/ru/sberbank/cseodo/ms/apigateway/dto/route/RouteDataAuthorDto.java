package ru.sberbank.cseodo.ms.apigateway.dto.route;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class RouteDataAuthorDto extends RouteDataDto{

    Object author;
}
