package ru.sberbank.cseodo.ms.apigateway.dto.numeration;

import lombok.Data;

@Data
public class NumerationDto {

    String documentKindId;
    String documentTypeId;
    String organizationId;
}
