package ru.sberbank.cseodo.ms.apigateway.mappers;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;

import java.util.Collections;
import java.util.Map;

@Slf4j
public abstract class JsonMapper {

    protected String map(Map<String, Object> value) {

        if (value != null) {
            try {
                return new ObjectMapper().writeValueAsString(value);
            } catch (Exception e) {
                log.warn("Error converting document item content json value to string: {}", value, e);
                return null;
            }
        }
        return null;
    }

    protected Map<String, Object> map(String value) {
        if (StringUtils.isEmpty(value)) {
            return Collections.emptyMap();
        }
        try {
            return new ObjectMapper().readValue(value, new TypeReference<Map<String, Object>>() {
            });
        } catch (Exception e) {
            log.warn("Error parsing document item content property to json: {}", value, e);
            return null;
        }
    }
}