package ru.sberbank.cseodo.ms.apigateway.dto.attachment;

import lombok.Data;

@Data
public class AttachmentRequestDTO {
    private String parentId;
}