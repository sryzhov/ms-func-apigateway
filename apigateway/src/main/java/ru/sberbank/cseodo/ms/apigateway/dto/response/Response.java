package ru.sberbank.cseodo.ms.apigateway.dto.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
public class Response<T> implements GatewayResponse {

    boolean success;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    T data;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    ErrorDto error;
}
