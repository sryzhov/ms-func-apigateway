package ru.sberbank.cseodo.ms.apigateway.mappers;

import org.mapstruct.Named;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
@Named("DictionaryConverter")
public class DictionaryConverter {

    @Named("objectToCode")
    public Object objectToCode(Object source) {
        if (source instanceof Map) {
            return ((Map)source).getOrDefault("code", "");
        }
        return null;
    }
}
