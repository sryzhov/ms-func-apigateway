package ru.sberbank.cseodo.ms.apigateway.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = HeaderValidator.class)
@Target({ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidHeader {

    String message() default "Неверный заголовок сообщения";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
