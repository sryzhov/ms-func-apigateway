package ru.sberbank.cseodo.ms.apigateway.dto.attachment;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;

@EqualsAndHashCode(callSuper = true)
@Data
public class EditorRequestDTO extends AttachmentRequestDTO {
    @NotNull
    private Integer majorVersion;
    @NotNull
    private Integer minorVersion;
}