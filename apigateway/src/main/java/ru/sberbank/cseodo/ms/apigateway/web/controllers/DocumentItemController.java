package ru.sberbank.cseodo.ms.apigateway.web.controllers;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import ru.sberbank.cseodo.ms.apigateway.dto.document.FrontDocumentItemFullDto;
import ru.sberbank.cseodo.ms.apigateway.dto.response.Response;
import ru.sberbank.cseodo.ms.apigateway.service.DocumentService;

import java.util.Map;
import java.util.UUID;

import static ru.sberbank.cseodo.ms.apigateway.utils.WebUtils.fixPostHeaders;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/web/v0.1/documents/{documentId}/items")
@AllArgsConstructor
@Slf4j
public class DocumentItemController {

    private final DocumentService documentService;

    @PostMapping
    public Response<String> addItem(@PathVariable(value = "documentId") UUID documentId,
                                           @RequestBody FrontDocumentItemFullDto itemDto,
                                           @RequestHeader Map<String, String> headerMap) {
        log.info("Got addItem request. documentId= {}, itemDto = {}", documentId, itemDto);
        Response<String> response = documentService.addItem(documentId, itemDto, fixPostHeaders(headerMap));
        log.info("Sending addItem response: {}", response);
        return response;
    }

    @PostMapping("{itemId}")
    public Response<String> editItem(@PathVariable(value = "documentId") UUID documentId,
                                            @PathVariable(value = "itemId") UUID itemId,
                                            @RequestBody FrontDocumentItemFullDto itemDto,
                                            @RequestHeader Map<String, String> headerMap) {
        log.info("Got editItem request. documentId= {}, itemId= {}, itemDto = {}", documentId, itemId, itemDto);
        Response<String> response = documentService.editItem(documentId, itemId, itemDto, fixPostHeaders(headerMap));
        log.info("Sending editItem response: {}", response);
        return response;
    }

    @DeleteMapping(value = "{itemId}")
    public Response<String> removeItem(@PathVariable(value = "documentId") UUID documentId,
                                              @PathVariable(value = "itemId") UUID itemId,
                                              @RequestHeader Map<String, String> headerMap) {
        log.info("Got removeItem request.  documentId= {}, itemId= {}", documentId, itemId);
        Response<String> response = documentService.removeItem(documentId, itemId, fixPostHeaders(headerMap));
        log.info("Sending removeItem response: {}", response);
        return response;
    }
}