package ru.sberbank.cseodo.ms.apigateway.mappers;

import java.util.UUID;

public interface UuidMapper {

    default String map(UUID value) {
        return value != null ? value.toString() : null;
    }

    default UUID map(String value) {
        return value != null ? UUID.fromString(value) : null;
    }
}