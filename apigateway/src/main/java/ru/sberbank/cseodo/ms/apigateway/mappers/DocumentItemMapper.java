package ru.sberbank.cseodo.ms.apigateway.mappers;

import org.mapstruct.InheritConfiguration;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.sberbank.cseodo.ms.apigateway.dto.document.BackDocumentItemDto;
import ru.sberbank.cseodo.ms.apigateway.dto.document.FrontDocumentItemDto;

import java.util.List;

@Mapper(componentModel = "spring")
public abstract class DocumentItemMapper extends JsonMapper {

    @Mapping(source = "guid", target = "itemGuid")
    @Mapping(source = "code", target = "itemCode")
    abstract BackDocumentItemDto frontToBack(FrontDocumentItemDto source);

    @InheritConfiguration
    public abstract List<BackDocumentItemDto> frontToBack(List<FrontDocumentItemDto> source);

    @InheritInverseConfiguration
    public abstract FrontDocumentItemDto backToFront(BackDocumentItemDto source);

    @InheritConfiguration
    public abstract List<FrontDocumentItemDto> backToFront(List<BackDocumentItemDto> source);
}