package ru.sberbank.cseodo.ms.apigateway.mappers;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.sberbank.cseodo.ms.apigateway.dto.document.BackDocumentDto;
import ru.sberbank.cseodo.ms.apigateway.dto.document.FrontDocumentDto;
import ru.sberbank.cseodo.ms.apigateway.dto.document.FrontDocumentRouteDto;

import java.util.List;

@Mapper(componentModel = "spring", uses = {DocumentItemMapper.class, DictionaryConverter.class})
public interface DocumentMapper extends UuidMapper {

    @Mapping(source = "guid", target = "documentGuid")
    @Mapping(source = "type", target = "documentType")
    @Mapping(source = "kind", target = "documentKind")
    @Mapping(source = "number", target = "documentNumber")
    BackDocumentDto frontToBack(FrontDocumentDto source);

    @InheritInverseConfiguration
    FrontDocumentDto backToFront(BackDocumentDto source);

    @InheritInverseConfiguration
    List<FrontDocumentDto> backToFront(List<BackDocumentDto> source);

    FrontDocumentRouteDto toRouteFront(FrontDocumentDto source);

    @Mapping(target = "documentType", qualifiedByName = {"DictionaryConverter", "objectToCode"})
    @Mapping(target = "documentKind", qualifiedByName = {"DictionaryConverter", "objectToCode"})
    @Mapping(target = "privacy", qualifiedByName = {"DictionaryConverter", "objectToCode"})
    @Mapping(target = "urgency", qualifiedByName = {"DictionaryConverter", "objectToCode"})
    BackDocumentDto backToBackWithDictToString(BackDocumentDto source);
}