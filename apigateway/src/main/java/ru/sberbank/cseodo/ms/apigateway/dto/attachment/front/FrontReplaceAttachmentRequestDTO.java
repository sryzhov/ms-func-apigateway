package ru.sberbank.cseodo.ms.apigateway.dto.attachment.front;

import lombok.Data;
import lombok.EqualsAndHashCode;
import ru.sberbank.cseodo.ms.apigateway.dto.attachment.ReplaceAttachmentRequestDTO;

@EqualsAndHashCode(callSuper = true)
@Data
public class FrontReplaceAttachmentRequestDTO extends ReplaceAttachmentRequestDTO {
    String parentType;
}
