package ru.sberbank.cseodo.ms.apigateway.utils;

import ru.sberbank.cseodo.ms.apigateway.dto.route.RouteDocumentDto;
import ru.sberbank.cseodo.ms.apigateway.dto.route.RouteDocumentVersionDto;
import ru.sberbank.cseodo.ms.apigateway.dto.route.RouteDto;
import ru.sberbank.cseodo.ms.apigateway.dto.route.RouteStatusDto;

import java.util.UUID;

public class RouteUtils {

    private static final RouteStatusDto STATUS_CREATED = new RouteStatusDto("preparing", "Подготавливается");
    private static final RouteDocumentVersionDto DEFAULT_VERSION = new RouteDocumentVersionDto(0, 0);

    private RouteUtils() {
    }

    public static RouteDto buildDefaultRoute(UUID documentId) {

        RouteDocumentDto documentDto = new RouteDocumentDto();
        documentDto.setGuid(documentId);
        documentDto.setVersion(DEFAULT_VERSION);

        RouteDto route = new RouteDto();
        route.setGuid(UUID.randomUUID());
        route.setStatus(STATUS_CREATED);
        route.setDocument(documentDto);

        return route;
    }
}
