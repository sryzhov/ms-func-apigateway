package ru.sberbank.cseodo.ms.apigateway.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;
import ru.sberbank.cseodo.ms.apigateway.dto.document.BackDocumentDto;
import ru.sberbank.cseodo.ms.apigateway.dto.document.BackDocumentItemDto;
import ru.sberbank.cseodo.ms.apigateway.dto.response.Response;

import java.util.List;
import java.util.Map;
import java.util.UUID;

@FeignClient(value = "${cseodo.service.document.name:document}/SberbankCSEODO/CSEODO/1/document",
        url = "${cseodo.service.document.url:}")
public interface DocumentApiClient {

    @PostMapping("create")
    void create(@RequestBody BackDocumentDto document,
                @RequestHeader Map<String, String> headerMap);

    @PostMapping("edit")
    void edit(@RequestBody BackDocumentDto document,
              @RequestHeader Map<String, String> headerMap);

    @GetMapping
    Response<BackDocumentDto> get(@RequestParam(value = "guid") UUID documentGuid,
                                  @RequestHeader Map<String, String> headerMap);

    @GetMapping("list")
    List<BackDocumentDto> list(@RequestParam(value = "limit") Long limit,
                               @RequestHeader Map<String, String> headerMap);

    @GetMapping("guid")
    Response<List<UUID>> idsList(@RequestParam(value = "limit") Long limit,
                                 @RequestHeader Map<String, String> headerMap);

    @PostMapping("delete")
    void delete(@RequestBody BackDocumentDto document,
                @RequestHeader Map<String, String> headerMap);

    @PostMapping("item/add")
    void addItem(@RequestBody BackDocumentItemDto itemDto,
                 @RequestHeader Map<String, String> headerMap);

    @PostMapping("item/edit")
    void editItem(@RequestBody BackDocumentItemDto itemDto,
                  @RequestHeader Map<String, String> headerMap);

    @PostMapping(value = "item/remove")
    void removeItem(@RequestBody BackDocumentItemDto itemDto,
                    @RequestHeader Map<String, String> headerMap);
}