package ru.sberbank.cseodo.ms.apigateway.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import ru.sberbank.cseodo.ms.apigateway.dto.response.Response;
import ru.sberbank.cseodo.ms.apigateway.dto.route.RouteDataAuthorDto;

import java.util.Map;

@FeignClient(value = "${cseodo.service.employee:employee}/api/web/v1/employee/",
        url = "${cseodo.service.employee.url:}")
public interface EmployeeApiClient {

    @GetMapping(value = "searchForSenders/")
    Response<Object> searchForSenders(@RequestParam(value = "query") String query,
                                  @RequestParam(value = "current", required = false) String current,
                                  @RequestParam(value = "count", required = false) String count,
                                  @RequestHeader Map<String, String> headerMap);

    @GetMapping(value = "searchForAddressees/")
    Response<Object> searchForAddressees(@RequestParam(value = "query") String query,
                                     @RequestParam(value = "current", required = false) String current,
                                     @RequestParam(value = "count", required = false) String count,
                                     @RequestHeader Map<String, String> headerMap);

    @GetMapping(value = "search/")
    Response<Object> search(@RequestParam(value = "id") Long id,
                                  @RequestParam(value = "variant", required = false) String variant,
                                  @RequestHeader Map<String, String> headerMap);

    @GetMapping(value = "getSigners/")
    Response<RouteDataAuthorDto> getSigners(@RequestParam(value = "id") Long id,
                                            @RequestHeader Map<String, String> headerMap);
}