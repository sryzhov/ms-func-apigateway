package ru.sberbank.cseodo.ms.apigateway.dto.document;

import lombok.Data;

import java.util.List;
import java.util.UUID;

@Data
public class BackDocumentDto {

    UUID documentGuid;
    Object documentType;
    Object documentKind;
    String documentNumber;
    String creationDate;
    Object privacy;
    Object urgency;
    UUID routeId;
    Integer minorVersion;
    Integer majorVersion;
    boolean deleted;
    List<BackDocumentItemDto> items;
}
