package ru.sberbank.cseodo.ms.apigateway.web.exceptionhandlers;

import com.netflix.client.ClientException;
import feign.FeignException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import ru.sberbank.cseodo.ms.apigateway.dto.response.Response;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import static ru.sberbank.cseodo.ms.apigateway.utils.WebUtils.buildErrorResponse;

@RestControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
@Slf4j
public class SpecificExceptionController {

    private static final String GATEWAY_ERROR_MESSAGE = "Gateway error";

    @ExceptionHandler(ClientException.class)
    public Response<Void> handleClientException(HttpServletRequest request, HttpServletResponse response, Throwable e) {

        log.error(GATEWAY_ERROR_MESSAGE, e);

        return buildErrorResponse(500, String.format("Ошибка микросервиса. %s", e.getMessage()));
    }

    @ExceptionHandler(AccessDeniedException.class)
    public Response<Void> handleAccessDeniedException(HttpServletRequest request, HttpServletResponse response, Throwable e) {

        log.error(GATEWAY_ERROR_MESSAGE, e);

        return buildErrorResponse(403, e.getMessage());
    }

    @ExceptionHandler(FeignException.NotFound.class)
    public Response<Void> handleFeignExceptionNotFound(HttpServletRequest request, HttpServletResponse response, Throwable e) {

        log.error(GATEWAY_ERROR_MESSAGE, e);

        return buildErrorResponse(404, String.format("Метод микросервиса не существует. %s", e.getMessage()));
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public Response<Void> handleConstraintViolationException(HttpServletRequest request, HttpServletResponse response, Throwable e) {

        log.error(GATEWAY_ERROR_MESSAGE, e);

        return buildErrorResponse(412, String.format("Ошибка валидации. %s", e.getMessage()));
    }
}
