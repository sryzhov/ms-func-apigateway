package ru.sberbank.cseodo.ms.apigateway.dto.attachment.front;

import lombok.Data;
import lombok.EqualsAndHashCode;
import ru.sberbank.cseodo.ms.apigateway.dto.attachment.UploadAttachmentRequestDTO;

@EqualsAndHashCode(callSuper = true)
@Data
public class FrontUploadAttachmentRequestDTO extends UploadAttachmentRequestDTO {
    String parentType;
}
