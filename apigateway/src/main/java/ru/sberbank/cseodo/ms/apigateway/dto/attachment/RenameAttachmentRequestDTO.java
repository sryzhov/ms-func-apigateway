package ru.sberbank.cseodo.ms.apigateway.dto.attachment;

import lombok.Data;

@Data
public class RenameAttachmentRequestDTO {
    private String parentId;
    private String renameTo;
}
