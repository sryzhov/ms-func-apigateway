package ru.sberbank.cseodo.ms.apigateway.config;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.access.AccessDecisionManager;
import org.springframework.security.access.AccessDecisionVoter;
import org.springframework.security.access.vote.AuthenticatedVoter;
import org.springframework.security.access.vote.RoleVoter;
import org.springframework.security.access.vote.UnanimousBased;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configurers.provisioning.InMemoryUserDetailsManagerConfigurer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import ru.sberbank.cseodo.ms.apigateway.enums.Role;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@EnableWebSecurity
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Bean
    public PasswordEncoder passwordEncoder() {
        return PasswordEncoderFactories.createDelegatingPasswordEncoder();
    }

    @Value("${cseodo.service.gateway.user.read:}")
    private String userRead;

    @Value("${cseodo.service.gateway.user.write:}")
    private String userWrite;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {

        InMemoryUserDetailsManagerConfigurer<AuthenticationManagerBuilder> authConfigurer = auth.inMemoryAuthentication();
        if (StringUtils.isNotEmpty(userWrite)) {
            authConfigurer.withUser(userWrite)
                    .password(passwordEncoder().encode("pass"))
                    .roles(Role.READER.getValue(), Role.WRITER.getValue());
        }
        if (StringUtils.isNotEmpty(userRead)) {
            authConfigurer.withUser(userRead)
                    .password(passwordEncoder().encode("pass"))
                    .roles(Role.READER.getValue());
        }
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers(HttpMethod.OPTIONS, "/**").permitAll()
                .anyRequest().authenticated()
//                .accessDecisionManager(accessDecisionManager())
                .and()
                .formLogin().disable()
                .csrf().disable()
                .httpBasic();
    }

    @Bean
    public AccessDecisionManager accessDecisionManager() {
        List<AccessDecisionVoter<?>> decisionVoters = Stream.of(
                new RoleVoter(), new AuthenticatedVoter()   //, new OpaVoter()
        ).collect(Collectors.toList());
        return new UnanimousBased(decisionVoters);
    }
}