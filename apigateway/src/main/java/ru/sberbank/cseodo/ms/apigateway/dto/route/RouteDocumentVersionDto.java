package ru.sberbank.cseodo.ms.apigateway.dto.route;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class RouteDocumentVersionDto {

    Integer major;
    Integer minor;
}
