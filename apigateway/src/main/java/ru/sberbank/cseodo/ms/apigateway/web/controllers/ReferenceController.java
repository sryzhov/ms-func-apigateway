package ru.sberbank.cseodo.ms.apigateway.web.controllers;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import ru.sberbank.cseodo.ms.apigateway.service.ReferenceService;

import java.util.Map;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api/web/v0.1/reference")
@AllArgsConstructor
@Slf4j
public class ReferenceController {

    private final ReferenceService referenceService;

    @GetMapping
    public Object getAll(@RequestHeader Map<String, String> headerMap) {

        log.info("Got getAll request. header = {}", headerMap);
        Object response = referenceService.getAll();
        log.info("Sending getAll response: {}", response);
        return response;
    }
}
