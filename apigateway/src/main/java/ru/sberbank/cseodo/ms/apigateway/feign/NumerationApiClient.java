package ru.sberbank.cseodo.ms.apigateway.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import ru.sberbank.cseodo.ms.apigateway.dto.numeration.NumerationDto;
import ru.sberbank.cseodo.ms.apigateway.dto.numeration.NumerationResult;
import ru.sberbank.cseodo.ms.apigateway.dto.response.Response;

@FeignClient(value = "${cseodo.service.numeration.name:numeration}/web/v1/services/sysNumber",
        url = "${cseodo.service.numeration.url:}")
public interface NumerationApiClient {

    @PostMapping(value = "nextValue")
    Response<NumerationResult> nextValue(@RequestParam(value = "organizationCode") String organizationCode,
                                         @RequestBody NumerationDto numeration);
}