package ru.sberbank.cseodo.ms.apigateway.dto.document;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.List;

@Data
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public class FrontDocumentDto {

    String guid;
    Object type;
    Object kind;
    String number;
    String creationDate;
    Object privacy;
    Object urgency;
    String routeId;
    Integer minorVersion;
    Integer majorVersion;
    boolean deleted;
    List<FrontDocumentItemDto> items;
}
