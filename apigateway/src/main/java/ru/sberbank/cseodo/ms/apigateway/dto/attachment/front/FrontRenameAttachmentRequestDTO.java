package ru.sberbank.cseodo.ms.apigateway.dto.attachment.front;

import lombok.Data;
import lombok.EqualsAndHashCode;
import ru.sberbank.cseodo.ms.apigateway.dto.attachment.RenameAttachmentRequestDTO;

@EqualsAndHashCode(callSuper = true)
@Data
public class FrontRenameAttachmentRequestDTO extends RenameAttachmentRequestDTO {
    String parentType;
}
