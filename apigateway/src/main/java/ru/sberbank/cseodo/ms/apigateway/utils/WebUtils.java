package ru.sberbank.cseodo.ms.apigateway.utils;

import ru.sberbank.cseodo.ms.apigateway.dto.response.ErrorDto;
import ru.sberbank.cseodo.ms.apigateway.dto.response.Response;

import java.util.Map;

public class WebUtils {

    //todo aspect?
    public static Map<String, String> fixPostHeaders(Map<String, String> headers) {

        headers.remove("content-length");
        return headers;
    }

    public static <T> Response<T> buildErrorResponse(int code, String message) {

        Response<T> errorResponse = new Response<>();
        errorResponse.setSuccess(false);
        errorResponse.setError(new ErrorDto(Integer.toString(code), message));

        return errorResponse;
    }

    public static <T> Response<T> buildSuccessResponse(T data) {

        Response<T> successResponse = new Response<>();
        successResponse.setSuccess(true);
        successResponse.setData(data);

        return successResponse;
    }
}
