import http from 'k6/http';

const get = ({ url, headers }, guid) => {

    const res = http.get(`${url}/api/web/v0.1/documents/${guid}`, { headers });

    const body = JSON.parse(res.body);
    return { body };
};

export default function (props, guid) {

    const limit = 50;
    let i = 0;
    let body = {};
    while (i < limit && !body.success) {
        body = get(props, guid).body;
        i++;
    }
    if (i === limit) {
        throw `Ну никак не получить документ id ${guid}`
    } else {
        if (i > 15) {
            console.log(`документ id ${guid} получен с ${i} раза`);
        }
    }
    return { body };
}
