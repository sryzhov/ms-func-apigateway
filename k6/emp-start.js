import http from 'k6/http';

export let options = {
    vus: 16,
    duration: "1m"
};
/*
export let options = {
    vus: 1,
    iterations: 1
};
*/

const props = {
    url: "http://localhost:8088"
}

export default function () {

    const res = http.get(props.url,
        {
            headers: {
                Authorization: "Basic d3JpdGVyOnBhc3M=",
            }
        });
        if(res.status !== 200){
            throw(`Упало. Код ${res.status}`);
        }
}

