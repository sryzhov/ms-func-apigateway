export function randomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
}

export function extractNumber(s) {
    const m = (s || "").match(/(\d+)/);
    return m ? m[0] : 0;
}

export function ramka(s, l) {

    const stars = "*".repeat(l);
    return "\n" + stars + "\n" + s.split("\n").map(row => "* " + row.padEnd(l - 4) + " *").join("\n") + "\n" + stars + "\n";
}