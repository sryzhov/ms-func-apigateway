import http from 'k6/http';
import uuid from "./uuid.js";

function randomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
}

const bodyCreate = () => ({
    documentGuid: uuid.v1(),
    documentType: "INTERNAL",
    documentKind: "MEMORANDUM",
    documentNumber: String(randomInt(999999)).padStart(6, "0"),
    privacy: "FOR_INTERNAL_USE",
    urgency: "URGENTLY",
    creationDate: "27.11.2020",
    "items": [
        {
            "itemGuid": uuid.v1(), "documentGuid": null, "itemCode": "SECTION", "sortOrder": 1.00,
            "encrypted": false, "content": ""
        }
    ]
});

export default function ({ url, headers }) {

    const body = bodyCreate();

    const res = http.post(`${url}/SberbankCSEODO/CSEODO/1/document/create`, JSON.stringify(body), { headers });
    //    console.log(JSON.stringify(res));

    return {
        guid: body.documentGuid
    };
};


