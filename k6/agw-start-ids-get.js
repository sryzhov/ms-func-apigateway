import ids from './agw-ids.js';
import get from './agw-get.js';

export let options = {
    vus: 16,
    duration: "1m"
};

const props = {
    url: "http://localhost:8088",
    headers: {
        transactionId: "00000000-0000-0000-0000-000000000000",
        userId: "1740572",
        Authorization: "Basic d3JpdGVyOnBhc3M=",
        "content-type": "application/json",
        Accept: "application/json",
        charset: "utf8"
    }
}

export function setup() {

    return ids(props);

}

export default function (data) {

    for (let i = 0; i < 1000; i++) {
        const res = get(props, data.body.data[i]);
        if(!res.body.success){
            throw("Сломалось.");
        };
    }

}

