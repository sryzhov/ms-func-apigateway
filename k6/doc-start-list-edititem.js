import list from './doc-list.js';
import edititem from './doc-edititem.js';
import description from "./doc-start-ids-edititem-description.js";

import { randomInt, extractNumber, ramka } from "./utils.js";

export let options = {
    vus: 1,
    duration: "1m",
};
/*
export let options = {
    vus: 1,
    iterations: 1
};
*/
const props = {
    url: "http://localhost:8089",
    headers: {
        transactionId: "00000000-0000-0000-0000-000000000000",
        "content-type": "application/json",
        Accept: "application/json",
        charset: "utf8"
    }
}

export function setup() {

    console.log(ramka(description, 150));
    return list(props);
}

export default function (data) {

    const list = data.body;

    const i = randomInt(list.length - 1);
    const body = list[i];
    const items = body.items.filter(item => item.itemCode === "paragraph");

    for (let j = 0; j < items.length; j++) {

        const x = extractNumber(items[j].content);
        const item = Object.assign({},
            items[j],
            { content: JSON.stringify({ value: `test doc content, edited ${+x + 1} times` }) }
        );
        const res = edititem(props, item);
        /*
                if (res.status !== 200) {
                    console.log(JSON.stringify(res));
                    throw ("Сломалось.");
                };
        */
    }

}

