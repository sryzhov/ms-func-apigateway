import http from 'k6/http';

const bodyCreate = () => ({
    type: "internal",
    kind: "memorandum"
});

export default function ({ url, headers }) {

    const requestBody = bodyCreate();

    const res = http.post(`${url}/api/web/v0.1/documents`, JSON.stringify(requestBody), { headers });

    const responseBody = JSON.parse(res.body);
    return {
        guid: responseBody.data.guid
    };
};


