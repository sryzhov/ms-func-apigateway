import create from './agw-create.js';
import get from './agw-get.js';

export let options = {
    vus: 16,
    duration: "1m"
};
/*
export let options = {
    vus: 1,
    iterations: 1
};
*/
const props = {
    url: "http://localhost:8088",
    headers: {
        transactionId: "00000000-0000-0000-0000-000000000000",
        userId: "1740572",
        Authorization: "Basic d3JpdGVyOnBhc3M=",
        "content-type": "application/json",
        Accept: "application/json",
        charset: "utf8"
    }
}

export default function () {

    const { guid } = create(props);
    get(props, guid);
}

