Для тестов надо установить:
- обязательно:
https://k6.io/docs/getting-started/installation
- желательно (для красивых графиков):
https://www.influxdata.com/products/influxdb/
https://grafana.com/grafana/download
в графану можно добавить готовый dashboard "k6 Load Testing Results" https://grafana.com/grafana/dashboards/2587

Запуск:
k6.exe run script.js  - выводится краткая справка в консоль
k6.exe run --out influxdb=http://localhost:8086/myk6db script.js 
- выводится краткая справка в консоль + можно разглядывать красивые графики в графане

Вместо script.js подставляем следующие файлы:
doc-start-*.js - обращение к МС Документ
agw-start-*.js - обращение к AGW

все другие скрипты напрямую не вызываются, они импортируются внутри *-start-*.js

В *-start-*.js обязательны переменные:

export let options = {
    vus: 1, 		// количество "виртуальных юзеров"
    duration: "1m", 	// продолжительность теста
//  либо
    iterations: 1	// количество итераций

};

const props = {		// и так понятно)
    url: "http://localhost:8089",	
    headers: {
        transactionId: "00000000-0000-0000-0000-000000000000",
        "content-type": "application/json",
        Accept: "application/json",
        charset: "utf8"
    }
}
