import create from './doc-create.js';
import get from './doc-get.js';

export let options = {
    vus: 1,
    iterations: 10
};

const props = {
    url: "http://localhost:8089",
    headers: {
        transactionId: "00000000-0000-0000-0000-000000000000",
        "content-type": "application/json",
        Accept: "application/json",
        charset: "utf8"
    }    
}

export default function () {

    const { guid } = create(props);
    get(props, guid);
}

