import http from 'k6/http';

export default function ({ url, headers }, body) {

    const res = http.post(`${url}/SberbankCSEODO/CSEODO/1/document/item/edit`, JSON.stringify(body), { headers });
    return res;
};


