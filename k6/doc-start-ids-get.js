import ids from './doc-ids.js';
import get from './doc-get.js';

export let options = {
    vus: 16,
    duration: "1m"
};

const props = {
    url: "http://localhost:8089",
    headers: {
        transactionId: "00000000-0000-0000-0000-000000000000",
        "content-type": "application/json",
        Accept: "application/json",
        charset: "utf8"
    }
}

export function setup() {

    return ids(props);

}

export default function (data) {

    for (let i = 0; i < 1000; i++) {
        const res = get(props, data.body.data[i]);
        if(!res.body.success){
            throw("Сломалось.");
        };
    }

}

