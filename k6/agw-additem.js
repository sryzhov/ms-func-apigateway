import http from 'k6/http';

const body = {
    code: "paragraph",
    sortOrder: 0,
    encrypted: false,
    content: { value: "test content agw" },
};

export default function ({ url, headers }, guid) {

    const res = http.post(`${url}/api/web/v0.1/documents/${guid}/items`, JSON.stringify(body), { headers });
    return JSON.parse(res.body);
};