import ids from './agw-ids.js';
import edititem from './agw-edititem.js';
import get from './agw-get.js';
import description from "./agw-start-ids-edititem-description.js";

import { randomInt, extractNumber, ramka } from "./utils.js";

export let options = {
    vus: 16,
    duration: "30s",
};
/*
export let options = {
    vus: 1,
    iterations: 1
};
*/
const props = {
    url: "http://localhost:8088",
    headers: {
        transactionId: "00000000-0000-0000-0000-000000000000",
        userId: "1740572",
        Authorization: "Basic d3JpdGVyOnBhc3M=",
        "content-type": "application/json",
        Accept: "application/json",
        charset: "utf8"
    }
}

export function setup() {

    console.log(ramka(description, 150));
    return ids(props);
}

export default function (data) {

    const ids = data.body.data;
    const i = randomInt(ids.length - 1);

    const { body } = get(props, ids[i]);
    const items = body.data.items.filter(item => item.code === "paragraph");

    for (let j = 0; j < items.length; j++) {

        const x = extractNumber((items[j].content || {}).value);
        const item = Object.assign({},
            items[j],
            { content: { value: `test agw content, edited ${+x + 1} times` } }
        );

        const res = edititem(props, ids[i], item);
        if (!res.success) {
            console.log(JSON.stringify(res));
            throw ("Сломалось.");
        };
    }

}

