import http from 'k6/http';

export default ({ url, headers }) => {

    const res = http.get(`${url}/api/web/v0.1/documents/ids?limit=1000`, { headers });

    const body = JSON.parse(res.body);
    return { body };
};
