import http from 'k6/http';

export default ({ url, headers }) => {

    const res = http.get(`${url}/SberbankCSEODO/CSEODO/1/document/list?limit=1000`, { headers });

    const body = JSON.parse(res.body);
    return { body };
};
