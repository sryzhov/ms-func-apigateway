import http from 'k6/http';

export default function ({ url, headers }, documentId, body) {

    const res = http.post(`${url}/api/web/v0.1/documents/${documentId}/items/${body.guid}`, JSON.stringify(body), { headers });
    return JSON.parse(res.body);
};