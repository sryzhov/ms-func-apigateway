import http from 'k6/http';
import uuid from "./uuid.js";

const body = documentGuid => ({
    itemGuid: uuid.v1(),
    documentGuid,
    itemCode: "paragraph",
    sortOrder: 0,
    encrypted: false,
    content: "test content doc",
});

export default function ({ url, headers }, guid) {

    const res = http.post(`${url}/SberbankCSEODO/CSEODO/1/document/item/add`, JSON.stringify(body(guid)), { headers });
    return res;
};


