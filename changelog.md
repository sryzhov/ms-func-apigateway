### Changelog

**28.01.2021:**
1. Документ. Поля `type`, `kind`, `privacy`, `urgency` - вместо строки, возвращается объект `{ code, description, short_description }`
2. Ограничение входных значений `type`, `kind`, `privacy`, `urgency`  
   type: `INTERNAL`, `INCOMING`, `OUTCOMING`  
   kind: `MEMORANDUM`  
   privacy: `FOR_INTERNAL_USE`, `FOR_OFFICIAL_USE`, `TRADE_SECRET`, `CONFIDENTIALLY`, `PUBLIC`  
   urgency: `URGENTLY`, `NOT_URGENTLY`  
3. Создаваемый документ содержит один элемент `{"code": "SECTION", "sortOrder": -1,}`
4. Адаптация к актуальным версиям МС Маршрут и Сотрудник
    
**22.01.2021:**
  
VC "Справочники" - ``/api/web/v0.1/reference/stamps`` заменен на ``/api/web/v0.1/reference`` 